
   <div class="container">
     <div class="row">
       
<h2 class="text-center">Add BLRequest Form</h2><hr>
<form class="" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<!-- // being shipper -->
 <fieldset class="shipper">
  <legend>Shipper:</legend>
<div class="form-group col-md-6">
<label for="shippername">Name<span>*</span>:</label>
<input type="text" class="form-control" name="shippername" id="shippername" value="">
<label for="shipperaddresstwo">Address 2:</label>
<textarea class="form-control" name="shipperaddresstwo" rows="2"></textarea>
</div>
<div class="form-group col-md-6">
<label for="shipperaddressone">Address:<span>*</span></label>
<textarea class="form-control" name="shipperaddressone" rows="2"></textarea>

<label for="shipperaddressthree">Address 3:</label>
<textarea class="form-control" name="shipperaddressthree" rows="2"></textarea>
</div>
</fieldset>
<!-- // end shipper -->

<!-- // being consignee -->
 <fieldset class="consignee">
  <legend>Consignee:</legend>
<div class="form-group col-md-6">
<label for="consigneename">Name<span>*</span>:</label>
<input type="text" class="form-control" name="consigneename" id="consigneename" value="">
<label for="consigneeaddresstwo">Address 2:</label>
<textarea class="form-control" name="consigneeaddresstwo" rows="2"></textarea>
</div>
<div class="form-group col-md-6">
<label for="consigneeaddressone">Address<span>*</span>:</label>
<textarea class="form-control" name="consigneeaddressone" rows="2"></textarea>

<label for="consigneeaddressthree">Address 3:</label>
<textarea class="form-control" name="consigneeaddressthree" rows="2"></textarea>
</div>
</fieldset>
<!-- // end consignee -->

<!-- // being notify -->
 <fieldset class="notify">
  <legend>Notify:</legend>
<div class="form-group col-md-6">
<label for="notifyname">Name<span>*</span>:</label>
<input type="text" class="form-control" name="notifyname" id="notifyname" value="">
<label for="notifyaddresstwo">Address 2:</label>
<textarea class="form-control" name="notifyaddresstwo" rows="2"></textarea>
</div>
<div class="form-group col-md-6">
<label for="notifyaddressone">Address<span>*</span>:</label>
<textarea class="form-control" name="notifyaddressone" rows="2"></textarea>

<label for="notifyaddressthree">Address 3:</label>
<textarea class="form-control" name="notifyaddressthree" rows="2"></textarea>
</div>
</fieldset>
<!-- // end notify -->

<!-- // being form detail -->
 <fieldset class="formdetail">
  <legend>Form Detail:</legend>
<div class="form-group col-md-6">
<label for="formeno">Form E No<span>*</span>:</label>
<input type="text" class="form-control" name="formeno" id="formeno" value="">
</div>
<div class="form-group col-md-6">
<label for="formedate">Form E Date<span>*</span>:</label>
<input type="text" class="form-control" name="formedate" id="formedate" value="">
</div>
</fieldset>
<!-- // end form detail -->


<!-- // being form Voyage -->
 <fieldset class="voyage">
  <legend>Voyage:</legend>
<div class="form-group col-md-3">
<label for="vessel">Vessel<span>*</span>:</label>
<input type="text" class="form-control" name="vessel" id="vessel" value="">
</div>
<div class="form-group col-md-3">
<label for="voyageno"> Voyage No<span>*</span>:</label>
<input type="text" class="form-control" name="voyageno" id="voyageno" value="">
</div>
<div class="form-group col-md-3">
<label for="FreightType"> Freight <span>*</span>:</label>
<select name="FreightType" id="FreightType"  class="form-control">
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
</div>
<div class="form-group col-md-3">
<label for="other" > &nbsp;</label>
<input type="text" class="form-control" name="other" id="other" value="">
</div>
</fieldset>
<!-- // end form Voyage -->

<!-- // being form location -->
 <fieldset class="location">
  <legend>Location:</legend>
<div class="form-group col-md-4">
<label for="portofloading">Port Of Loading<span>*</span>:</label>
<select name="portofloading" id="portofloading"  class="form-control">
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
</div>
<div class="form-group col-md-4">
<label for="portofdischarge"> Port of Discharge<span>*</span>:</label>
<select name="portofdischarge" id="portofdischarge"  class="form-control">
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
</div>
<div class="form-group col-md-4">
<label for="placeofdelivery"> Place of Delivery<span>*</span>:</label>
<select name="placeofdelivery" id="placeofdelivery"  class="form-control">
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
</div>

</fieldset>
<!-- // end form location -->



<!-- // being form Number Of Original BL -->
 <fieldset class="location">
  <legend>Number Of Original BL:</legend>
<div class="form-group col-md-4">
<label for="blnumber">BL Number<span>*</span>:</label>
<select name="blnumber" id="blnumber"  class="form-control">
  <option value="1">1</option>
  <option value="2">2</option>
   <option value="3">3</option>

</select>
</div>
<div class="form-group col-md-4">
<label for="commodity"> Commodity<span>*</span>:</label>
<input type="text" class="form-control" name="commodity" id="commodity" value="">
</div>
<div class="form-group col-md-4">
<label for="packagestype"> Packages Type<span>*</span>:</label>
<select name="packagestype" id="packagestype"  class="form-control">
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
</div>

</fieldset>
<!-- // end form Number Of Original BL -->


<!-- // being Container -->
 <fieldset class="container">
  <legend>Containers:</legend>
<div class="col-md-6" >

<div class="row">
  <input type="button" name="" id="add" value="+" class="btn btn-primary">
<div class="row-add" id="add1">
  <div class="form-group col-md-4">
<label for="containerno"> Container Number <span>*</span>:</label>
<input type="text" class="form-control containerno" name="containerno[]" value="">
</div>
<div class="form-group col-md-4">
<label for="sealnumber"> Seal Number<span>*</span>:</label>
<input type="text" class="form-control sealnumber" name="sealnumber[]"  value="">
</div>
<div class="form-group col-md-4">
<label for="noofpackages"> No. Of Packages<span>*</span>:</label>
<input type="text" class="form-control noofpackages" name="noofpackages[]"  value="">
</div>


</div>

<!-- <a id="delete_row" class="btn btn-danger  btn-xs">Delete Row</a> -->
</div>
<div id="tab_containers"></div>

</div>

<div class=" col-md-6" id="tab_mark">

<div class="row">
  <div class="row-add" id="addrr0">
  <div class="form-group col-md-12" >
<label for="marksno">Marks &amp; No.<span>*</span>:</label>
<input type="text" class="form-control" name="marksno" id="marksno" value="">
</div>
</div>
</div>
<a id="add_rows" class="btn btn-default">Add Row</a>
<a id="del_row" class="btn btn-danger">Delete Row</a>
</div>

</fieldset>
<!-- // end Container -->



<!-- // being Description Of Goods -->
 <fieldset class="container">
  <legend>Description Of Goods:</legend>
<div class="form-group col-md-12">
<label for="description" > &nbsp;</label>
<textarea class="form-control" name="description" rows="2"></textarea>
</div>
</fieldset>
<!-- // end Description Of Goods -->

<!-- // being Weight -->
 <fieldset class="container">
  <legend>Weight:</legend>
<div class="form-group col-md-4">
<label for="description" > Net Weight</label>
<input type="text" class="form-control" name="marksno" id="marksno" value="">
<span> kg</span>
</div>
<div class="form-group col-md-4">
<label for="description" > Gross Weigh</label>
<input type="text" class="form-control" name="marksno" id="marksno" value="">
<span> kg</span>
</div>
</fieldset>
<!-- // end Weight -->



<div class="form-group pull-right">
  <a href="products.php" class="btn btn-default">Cancel</a>
<input type="submit" value=" Preview BL" class="btn btn-success">
</div><div class="clearfix"></div>
</form>


     </div>
   </div>