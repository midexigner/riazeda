<?php
if(is_file("../include/inc_head.php")){
require_once("../include/inc_head.php");
}else{echo "please check the configure files"; }
if($_SESSION['clogged_in'] == false){redirect(BASE_URL.'/clients/');}
$exportQ  = "SELECT id, bookingstatus, cargostatus, shipper,forwarder,shippingline,status FROM exportsearchengine";
$exportRun = mysqli_query($db,$exportQ);
$count = mysqli_num_rows($exportRun);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Export Search Engine</title>
	<?php 
	require_once '../include/inc_css_base.php'; 
	require_once '../include/inc_css_base_clients.php';
	?>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/include/css/bootgrid/jquery.bootgrid.min.css">


	 <?php 
   require_once '../include/inc_js_base_client.php';
	 require_once '../include/inc_js_base_client.php';
	 ?> 
   <script src="<?php echo get_template_directory_uri(); ?>/include/js/bootgrid/jquery.bootgrid.js"></script>
</head>
<body>
<?php require_once 'header.php'; ?>

<?php require_once 'sidebar.php'; ?>

		<div class="col-md-10 content">
            <div class="panel panel-default">
                <div class="panel-heading  text-center">
                   <h2> Export Search Engine List</h2>
                  
                </div>
                <div class="panel-body">
                  	
  <table  id="export_data" class="table table-striped table-bordered table-hover table-condensed">
    <thead>
       <th data-column-id="data-column-id" data-type="numeric">Id</th>
       <th data-column-id="shipper">Shipper</th>
       <th data-column-id="forwarder">Forwarder</th>
      <th data-column-id="booking_status">Booking Status</th>
      <th data-column-id="cargo_status">Cargo Status</th>
      <th data-column-id="shipping_line">Shipping Line</th>
      <th data-column-id="status" data-formatter="status" data-sortable='false'>Status</th>
      
    </thead>
    <tbody>
      <?php if($count >0 ){ 
while ($export = mysqli_fetch_assoc($exportRun)) {
  # code...

        ?>
      <tr>
        <td><?php echo $export['id']; ?></td>
         <td><?php echo $export['shipper']; ?></td>
          <td><?php echo $export['forwarder']; ?></td>
           <td><?php echo $export['bookingstatus']; ?></td>
            <td><?php echo $export['cargostatus']; ?></td>
            <td><?php echo (($export['shippingline'])=='1'?'Wan Hei':'Balaji'); ?></td>
            <td valign="middle"><?php echo (($export['status'])=='complete'?'<i class="label label-success">Complete Progress</label>':'<a href="'.BASE_URL.'/clients/blform.php?id='.$export['id'].'" class=" btn btn-xs btn-danger"><i class="label label-danger"> Incomplete Progress </label></a>'); ?></td>
            
      </tr>
      <?php  } }else{ ?>
 <tr class="danger text-center">
        <td colspan="7" class="text-danger"><h2>Export Search Engine Not Found</h2></td>
        
      </tr>
      <?php } ?>
    </tbody>
   
  </table>
                </div>
            </div>
		</div>
		

	
<script>
  
   $('.form').attr('data-toggle="validator"')
       $("input,textarea,select").prop('required',true);
       $('.form').validator();

    var exportData = $("#export_data").bootgrid({
      ajax:true,
      rowSelect:true,
      post:function(){
        return {
          id: "b0df282a-06d67-40e5-8558-c9e93b7befed"
        };
      },
      url:'action.php',
      formatters:{
        "commands": function(column,row){
          return "<button type='button' class='btn btn-warning btn-xs update' data-row-id='"+row.product_id+"'>Edit</button>"+ " &nbsp; <button type='button' class='btn btn-danger btn-xs delete' data-row-id='"+row.product_id+"'>Delete</button>"
        }
      }
    });

    //$("#export_data").bootgrid();
</script>
<?php require_once 'footer.php'; ?>
	
</body>
</html>