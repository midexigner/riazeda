<?php
if(is_file("../include/inc_head.php")){
require_once("../include/inc_head.php");
}else{
  echo "please check the configure files"; 
}

                
if(isset($_POST['register'])){
$fullname = db_escape($db,$_POST['name']);
$username = db_escape($db,$_POST['username']);
$email = db_escape($db,$_POST['email']);
$contact = db_escape($db,$_POST['contact']);
$password = db_escape($db,$_POST['password']);
$password_confirmation = db_escape($db,$_POST['password_confirmation']);
$hostname = gethostname();
#$name = "/^[A-Z][a-zA-z ]+$/";
#$password = rand(1000,5000);
$name = "/(.[A-Z][a-zA-z ]), (.*)/";
$usernameCheck = "/^[a-zA-Z][a-zA-Z0-9-_\.]+$/";
$emailValidation = "/^[_a-z0-9-]+(\.[_a-z0-9-])*@[a-z0-9]+(\.[a-z]{2,4})$/";
$number = "/^[0-9]+$/";

  if(empty($fullname) || empty($username) || empty($email) || empty($password) || empty($password_confirmation) || empty($password_confirmation)){
    echo '<div class="alert alert-danger alert-dismissable">Please fill all fields</div>';
  }else{

    if(!(strlen($fullname < 4))){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    {$fullname} is not less 4 digit </div>";
  
}


   if(!preg_match($usernameCheck, $username)){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    {$username} is not valid </div>";
   
}
/*if(!preg_match($emailValidation,$email)){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    {$email}is not valid </div>";
   
}*/


if(strlen($password) < 6 ){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    Password is weak </div>";
    
}
if($password != $password_confirmation){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    Password is not same</div>";

}

/*if(!preg_match($number,$contact)){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    Mobile Number {$contact} is not valid </div>";
   
}*/
/*if(!(strlen($contact) == 10 )){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    Mobile number must be 10 digit </div>";
    
}*/

 // existing email address in our database
    // existing email address in our database
$sql = "SELECT id FROM registered WHERE email = '$email' LIMIT 1";
$check = mysqli_query($db,$sql);
$count_email = mysqli_num_rows($check);
    if($count_email > 0){
    echo "<div class=\"alert alert-warning\" role=\"alert\">
    Email address is already available try another email address</div>";
    
}else{

$hash = md5( rand(0,1000) );
$password_hashed = md5($password);

$sql = "INSERT INTO `registered` (name,email,password,contact,username,hash,status,hostname) VALUES ('$fullname','$email','$password_hashed','$contact','$username','$hash','inactive','$hostname')";
        $run_query = mysqli_query($db,$sql);
        if($run_query){
    echo "<div class=\"alert alert-success\" role=\"alert\">
    Your account has been made, <br /> please verify it by clicking the activation link that has been send to your email.</div>";
    mailing_registration("career@zakutaa.com","Riazeda",$email,$username,$password,$hash);
        }else{
           echo "<div class=\"alert alert-danger\" role=\"alert\">
    Your are Registered Failed </div>";
     
        }
}

  }
  

  //var_dump($_POST);

}

//mailing("career@zakutaa.com","Riazeda","midrees.cis@gmail.com",'testcis','123','hash');
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Register form</title>
  <?php require_once '../include/inc_css_base.php'; ?>

  <style>
    
    /* Credit to bootsnipp.com for the css for the color graph */
.colorgraph {
  height: 5px;
  border-top: 0;
  background: #c4e17f;
  border-radius: 5px;
  background-image: -webkit-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -moz-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: -o-linear-gradient(left, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
  background-image: linear-gradient(to right, #c4e17f, #c4e17f 12.5%, #f7fdca 12.5%, #f7fdca 25%, #fecf71 25%, #fecf71 37.5%, #f0776c 37.5%, #f0776c 50%, #db9dbe 50%, #db9dbe 62.5%, #c49cde 62.5%, #c49cde 75%, #669ae1 75%, #669ae1 87.5%, #62c2e4 87.5%, #62c2e4);
}
  </style>
</head>
<body>
<div class="container">

<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
      <h2>Please Sign Up <small>It's free and always will be.</small></h2>
      <hr class="colorgraph">
     
          <div class="form-group">
                <input type="text" name="name" id="name" class="form-control input-lg" placeholder="Full Name" tabindex="1">
          </div>

          <div class="form-group">
        <input type="text" name="username" id="username" class="form-control input-lg" placeholder="Username" tabindex="2">
      </div>
        
          <div class="form-group">
            <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email Address" tabindex="3">
          </div>
        
      
      <div class="form-group">
        <input type="tel" name="contact" id="contact" class="form-control input-lg" placeholder="Phone Number" tabindex="4">
      </div>
     
          <div class="form-group">
            <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password" tabindex="5">
          </div>
        
          <div class="form-group">
            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-lg" placeholder="Confirm Password" tabindex="6">
          </div>
        
      
      <hr class="colorgraph">
      
        <input type="submit" value="Register" name="register" class="btn btn-primary btn-block btn-lg" tabindex="7">
       
      </div>
      </div>
    </form>
  </div>
</div>


</div>

 



 <?php require_once '../include/inc_js_base_client.php';?> 
</body>
</html>










