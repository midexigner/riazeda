<?php
if(is_file("../include/inc_head.php")){
require_once("../include/inc_head.php");
}else{
  echo "please check the configure files"; 
}

//vd($_SESSION);
if($_SESSION['clogged_in'] == false){
	redirect(BASE_URL.'/clients/');
}
if(isset($_POST['exportsearch'])){
$cargotype = db_escape($db,$_POST['cargotype']);
$bookingstatus = db_escape($db,$_POST['bookingstatus']);
$cargostatus = db_escape($db,$_POST['cargostatus']);
$commodity = db_escape($db,$_POST['commodity']);
$pol = db_escape($db,$_POST['pol']);
$pod = db_escape($db,$_POST['pod']);
$requestform = db_escape($db,$_POST['requestform']);
$shipper = db_escape($db,$_POST['shipper']);
$forwarder = db_escape($db,$_POST['forwarder']);
$clearingagent = db_escape($db,$_POST['clearingagent']);
$emptyfrom = db_escape($db,$_POST['emptyfrom']);
$total20 = db_escape($db,$_POST['total20']);
$total40 = db_escape($db,$_POST['total40']);
$totalrf = db_escape($db,$_POST['totalrf']);
$referremarks = db_escape($db,$_POST['referremarks']);
$shippingline = db_escape($db,$_POST['shippingline']);
$clientsid = db_escape($db,$_SESSION['cid']);

$sql = "INSERT INTO `exportsearchengine` (cargotype,bookingstatus,cargostatus,commodity,pol,pod,requestform,shipper,forwarder,clearingagent,emptyfrom,total20,total40,totalrf,referremarks,shippingline,status,clientsid) VALUES ('$cargotype','$bookingstatus','$cargostatus','$commodity','$pol','$pod','$requestform','$shipper','$forwarder',clearingagent,'$emptyfrom','$total20','$total40','$totalrf','$referremarks','$shippingline','incomplete','$clientsid')";
        $run_query = mysqli_query($db,$sql);
        $id = mysqli_insert_id($db);
        $exportQ =  "SELECT id,shippingline FROM exportsearchengine WHERE id = '$id'";
        $query = mysqli_query($db,$exportQ);
        $exportRow = mysqli_fetch_assoc($query);
        $_SESSION['shippingline'] = $exportRow['shippingline'];
        $_SESSION['exportsid'] = $exportRow['id'];
        if($exportRow['shippingline'] == 1){
          redirect('bpl.php');
        }else if($exportRow['shippingline'] == 2){
            redirect('bpl.php');
        }
      
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Export Search Engine</title>
	<?php 
	require_once '../include/inc_css_base.php'; 
	require_once '../include/inc_css_base_clients.php';

	?>


	 <?php require_once '../include/inc_js_base_client.php';
	 require_once '../include/inc_js_base_client.php';

	 ?> 
</head>
<body>
<?php require_once 'header.php'; ?>

<?php require_once 'sidebar.php'; ?>

		<div class="col-md-10 content">
            <div class="panel panel-default">
                <div class="panel-heading  text-center">
                   <h2> Export Search Engine</h2>
                    <!-- <a href="#" class="btn btn-success btn-sm pull-right" id="add-product-btn">View List</a><div class="clearfix"></div> -->
                </div>
                <div class="panel-body">
                  	<div id="exportForm">
                  		<form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
<div class="col-md-12">
<div class="row">
  
   <div class="form-group col-md-4">
<label for="cargotype">Cargo Type:</label>
<select class="form-control" id="cargotype" name="cargotype" placeholder="Please Select Cargo Type"  >
<option value="" selected=""></option>
  <option value="Refer">Refer</option>
  <option value="Dry">Dry</option>
  
</select>
 <div class="help-block with-errors"></div>
</div>
 <div class="form-group col-md-4">
<label for="bookingstatus">Booking  Status:</label>
<select class="form-control" id="bookingstatus" name="bookingstatus" placeholder="Please Select Booking  Status">
<option value="" selected=""></option>
  <option value="Prepaid">Prepaid</option>
   <option value="Collect">Collect</option>
  
</select>
 <div class="help-block with-errors"></div>
</div>


<div class="form-group col-md-4">
<label for="cargostatus">Cargo Status:</label>
<input type="text" class="form-control" name="cargostatus" id="cargostatus" placeholder="Cargo Status" value="">
 <div class="help-block with-errors"></div>
</div>
</div>
<!-- // end row -->
<div class="row">
  
   <div class="form-group col-md-4">
<label for="commodity">Commodity:</label>
<input type="text" class="form-control" id="commodity" name="commodity" placeholder="Commodity" value="">
 <div class="help-block with-errors"></div>
</div>
 <div class="form-group col-md-4">
<label for="pol">P.O.L:</label>
<input type="text" class="form-control" id="pol" name="pol" placeholder="P.O.L" value="">
 <div class="help-block with-errors"></div>
</div>


<div class="form-group col-md-4">
<label for="pod">P.O.D:</label>
<input type="text" class="form-control" name="pod" id="pod" placeholder="P.O.D" value="">
 <div class="help-block with-errors"></div>
</div>
</div>

<!-- // end row -->

<div class="row">
  
   <div class="form-group col-md-4">
<label for="requestform">Request Form:</label>
<select class="form-control" id="requestform" name="requestform" placeholder="Please Select Request Form">
<option value="" selected=""></option>
  <option value="Clearing Agent">Clearing Agent</option>
   <option value="Shiper">Shiper</option>
   <option value="Forwder">Forwder</option>
   <option value="Self">Self</option>
  
</select>
 <div class="help-block with-errors"></div>
</div>

</div>

<!-- // end row -->
<div class="row">
   <div class="form-group col-md-6">
<label for="shipper">Shipper:</label>
<input type="text" class="form-control" id="shipper" name="shipper" placeholder="Shipper" value="">
 <div class="help-block with-errors"></div>
</div>


<div class="form-group col-md-6">
<label for="forwarder">Forwarder:</label>
<input type="text" class="form-control" name="forwarder" id="forwarder" placeholder="Forwarder" value="">
 <div class="help-block with-errors"></div>
</div>
</div>
<!-- // end row -->

<div class="row">
   <div class="form-group col-md-6">
<label for="clearingagent">Clearing Agent:</label>
<input type="text" class="form-control" id="clearingagent" name="clearingagent" placeholder="Clearing Agent" value="">
 <div class="help-block with-errors"></div>
</div>


<div class="form-group col-md-6">
<label for="emptyfrom">Empty From:</label>
<input type="text" class="form-control" name="emptyfrom" id="emptyfrom" placeholder="Empty From" value="">
 <div class="help-block with-errors"></div>
</div>
</div>
<!-- // end row -->

<div class="row">
  
   <div class="form-group col-md-4">
<label for="total20">Total 20':</label>
<input type="text" class="form-control" id="total20" name="total20" placeholder="Total 20'" value="">
 <div class="help-block with-errors"></div>
</div>
 <div class="form-group col-md-4">
<label for="total40">Total 40':</label>
<input type="text" class="form-control" id="total40" name="total40" placeholder="Total 40'" value="">
 <div class="help-block with-errors"></div>
</div>


<div class="form-group col-md-4">
<label for="totalrf">Total RF':</label>
<input type="text" class="form-control" name="totalrf" id="totalrf" placeholder="Total RF'" value="">
 <div class="help-block with-errors"></div>
</div>
</div>

<!-- // end row -->


<div class="row">
   <div class="form-group col-md-12">
<label for="referremarks">Refeer Remarks:</label>
<input type="text" class="form-control" id="referremarks" name="referremarks" placeholder="Refeer Remarks" value="">
 <div class="help-block with-errors"></div>
</div>

</div>
<!-- // end row -->


<div class="row">
   <div class="form-group col-md-6">
<label for="shippingline">Shipping Line <span class="req">*</span>:</label>
<select class="form-control" id="shippingline" name="shippingline" placeholder="Please Select Shipping Line">
<option value="" selected=""></option>
  <option value="1">WN HAI</option>
   <option value="2">BALAJI</option>
   
  
</select>
 <div class="help-block with-errors"></div>
</div>

</div>
<!-- // end row -->

</div>

<div class="row text-center">
  <div class="form-group">
  <a href="" class="btn btn-default">Cancel</a>
<input type="submit" value="Save" name="exportsearch" class="btn btn-success">
</div>

</div>

<div class="clearfix"></div>

                  	</form>
		
	</div>

                </div>
            </div>
		</div>
		

	
<script>
  
   $('.form').attr('data-toggle="validator"')
       $("input,textarea,select").prop('required',true);
       $('.form').validator();
</script>
<?php require_once 'footer.php'; ?>
	
</body>
</html>