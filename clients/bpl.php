<?php
if(is_file("../include/inc_head.php")){
require_once("../include/inc_head.php");
}else{
  echo "please check the configure files"; 
}

//pr($_SESSION);
if($_SESSION['clogged_in'] == false){
	redirect(BASE_URL.'/clients/');
}else if(isset($_SESSION['shippingline']) != 1 || isset($_SESSION['shippingline']) != 2){
  redirect(BASE_URL.'/clients/export.php');
}


if(isset($_POST['previewbl'])){
  //vd($_POST);
  $exportsid = db_escape($db,$_SESSION['exportsid']);
  $shippername = db_escape($db,$_POST['shippername']);
  $shipperaddressone = db_escape($db,$_POST['shipperaddressone']);
  $shipperaddresstwo = db_escape($db,$_POST['shipperaddresstwo']);
  $shipperaddressthree = db_escape($db,$_POST['shipperaddressthree']);
  $consigneename = db_escape($db,$_POST['consigneename']);
  $consigneeaddressone = db_escape($db,$_POST['consigneeaddressone']);
  $consigneeaddresstwo = db_escape($db,$_POST['consigneeaddresstwo']);
  $consigneeaddressthree = db_escape($db,$_POST['consigneeaddressthree']);
  $notifyname = db_escape($db,$_POST['notifyname']);
  $notifyaddressone = db_escape($db,$_POST['notifyaddressone']);
  $notifyaddresstwo = db_escape($db,$_POST['notifyaddresstwo']);
  $notifyaddressthree = db_escape($db,$_POST['notifyaddressthree']);
  $formeno = db_escape($db,$_POST['formeno']);
  $formedate = db_escape($db,$_POST['formedate']);
  $vessel = db_escape($db,$_POST['vessel']);
  $voyageno = db_escape($db,$_POST['voyageno']);
  $FreightType = db_escape($db,$_POST['FreightType']);
  $other = db_escape($db,$_POST['other']);
  $portofloading = db_escape($db,$_POST['portofloading']);
  $portofdischarge = db_escape($db,$_POST['portofdischarge']);
  $placeofdelivery = db_escape($db,$_POST['placeofdelivery']);
  $blnumber = db_escape($db,$_POST['blnumber']);
  $commodity = db_escape($db,$_POST['commodity']);
  $packagestype = db_escape($db,$_POST['packagestype']);
  $containernumber = $_POST['containernumber'];
  $containernumber_escaped_values = array_map('mysql_real_escape_string', array_values( $containernumber));
  $containernumber_values  = implode("\", \"", $containernumber_escaped_values);
  $sealnumber = $_POST['sealnumber'];
  $sealnumber_escaped_values = array_map('mysql_real_escape_string', array_values( $sealnumber));
  $sealnumber_values  = implode("\", \"", $sealnumber_escaped_values);
  $noofpackages = $_POST['noofpackages'];
  $noofpackages_escaped_values = array_map('mysql_real_escape_string', array_values( $noofpackages));
  $noofpackages_values  = implode("\", \"", $noofpackages_escaped_values);
  $marksno = $_POST['marksno'];
  $marksno_escaped_values = array_map('mysql_real_escape_string', array_values( $marksno));
  $marksno_values  = implode("\", \"", $marksno_escaped_values);
  $description = $_POST['description'];
  $description_escaped_values = array_map('mysql_real_escape_string', array_values( $description));
  $description_values  = implode("\", \"", $description_escaped_values);
  vd($description_values);
  $netweight = db_escape($db,$_POST['netweight']);
  $grossweight = db_escape($db,$_POST['grossweight']);
  $measurement = db_escape($db,$_POST['measurement']);
  $client_id = db_escape($db,$_SESSION['cid']);
  $shippingline = db_escape($db,$_SESSION['shippingline']);
$msg='';
  if(empty($shippername) && empty($shipperaddressone)  || empty($consigneename) || empty($consigneeaddressone) || empty($notifyname) || empty($notifyaddressone) || empty($formeno)  || empty($formedate) || empty($vessel)  || empty($voyageno) || empty($FreightType) || empty($portofloading)  || empty($portofdischarge)  || empty($placeofdelivery) || empty($netweight) || empty($grossweight) || empty($measurement)){
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Shipper Name</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Shipper Address Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Consignee Name Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Consignee Address Required</div>';
   $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Notify Name Required</div>';
   $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Notify Address Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Form E Number Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Form E Date Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Vessel Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Voyage Number Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Freight Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Port Of Loading Required</div>';
   $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Port Of Dischage Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Place Of Delivery Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Net Weight Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Gross Weight Required</div>';
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Measurement Required</div>';
  }elseif(empty($shippername)){
    $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Shipper Name</div>';
  }elseif(empty($shipperaddressone)){
    $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Shipper Address Required</div>';
  }
  elseif(empty($consigneename)){
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Consignee Name Required</div>';
  }
  elseif(empty($consigneeaddressone)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Consignee Address Required</div>';
  }
  elseif(empty($notifyname)){
  $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Notify Name Required</div>';
  }
  elseif(empty($notifyaddressone)){
  $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Notify Address Required</div>';
  }
   elseif(empty($formeno)){
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Form E Number Required</div>';
  }
   elseif(empty($formedate)){
     $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Form E Date Required</div>';
  }
   elseif(empty($vessel)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Vessel Required</div>';
  }
   elseif(empty($voyageno)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Voyage Number Required</div>';
  }
   elseif(empty($FreightType)){
   $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Freight Required</div>';
  }
   elseif(empty($portofloading)){
     $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Port Of Loading Required</div>';
  }
   elseif(empty($portofdischarge)){
   $msg .=  '<div class="alert alert-danger alert-dismissable">Please Enter Port Of Dischage Required</div>';
  }
   elseif(empty($placeofdelivery)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Place Of Delivery Required</div>';
  }
   elseif(empty($netweight)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Net Weight Required</div>';
  }
   elseif(empty($grossweight)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Gross Weight Required</div>';
  }
   elseif(empty($measurement)){
  $msg .= '<div class="alert alert-danger alert-dismissable">Please Enter Measurement Required</div>';
  }else{
    echo $sql = "INSERT INTO `blform` (`exportsid`, `shippername`, `shipperaddressone`, `shipperaddresstwo`, `shipperaddressthree`, `consigneename`, `consigneeaddressone`, `consigneeaddresstwo`, `consigneeaddressthree`, `notifyname`, `notifyaddressone`, `notifyaddresstwo`, `notifyaddressthree`, `formeno`, `formedate`, `vessel`, `voyageno`, `FreightType`, `other`, `portofloading`, `portofdischarge`, `placeofdelivery`, `blnumber`, `commodity`, `packagestype`, `containernumber`, `sealnumber`, `noofpackages`, `marksno`, `description`, `netweight`, `grossweight`, `measurement`, `client_id`, `shippingline`) VALUES
('$exportsid', '$shippername', '$shipperaddressone', '$shipperaddresstwo', '$shipperaddressthree', '$consigneename', '$consigneeaddressone', '$consigneeaddresstwo', '$consigneeaddressthree', '$notifyname', '$notifyaddressone', '$notifyaddresstwo', '$notifyaddressthree', '$formeno', '$formedate', '$vessel', '$voyageno', '$FreightType', '$other', '$portofloading', '$portofdischarge', '$placeofdelivery', '$blnumber', '$commodity', '$packagestype', '$containernumber_values', '$sealnumber_values', '$noofpackages_values', '$marksno_values', '$description_values', '$netweight', '$grossweight', '$measurement', '$client_id', '$shippingline')";

   $run_query = mysqli_query($db,$sql);
   $id = mysqli_insert_id($db);
   $bplQ =  "SELECT id,exportsid FROM blform WHERE id = '$id'";
   $query = mysqli_query($db,$bplQ);
   $bplRow = mysqli_fetch_assoc($query);
   $iidd = $bplRow['id'];
   mysqli_query($db,"UPDATE exportsearchengine SET status = 'complete' where id = '$iidd'");
  unset($_SESSION['exportsid']);
   redirect('exportList.php');
#$msg .= '<div class="alert alert-success alert-dismissable">Successfull form submit</div>';
  }

 
 
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> BPL Form</title>
	<?php 
	require_once '../include/inc_css_base.php'; 
	require_once '../include/inc_css_base_clients.php';
	?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/include/clients/css/bplform.css">

	 <?php require_once '../include/inc_js_base_client.php';
	 require_once '../include/inc_js_base_client.php';

	 ?> 
</head>
<body>
<?php require_once 'header.php'; ?>

<?php require_once 'sidebar.php'; ?>

		<div class="col-md-10 content">
            <div class="panel panel-default">
                <div class="panel-heading  text-center">
                    <h2>Add BLRequest Form</h2>
                    <!-- <a href="#" class="btn btn-success btn-sm pull-right" id="add-product-btn">View List</a><div class="clearfix"></div> -->
                </div>
                <div class="panel-body">
                    <?php if(isset($msg)){ echo $msg; } ?>
                  	<div id="bplForm">
                  		<form class="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
<!-- // being shipper -->
 <fieldset class="shipper">
  <legend>Shipper:</legend>
<div class="form-group col-md-6">
<label for="shippername">Name<span>*</span>:</label>
 <div class="help-block with-errors"></div>
<input type="text" class="form-control req" name="shippername" id="shippername" value="">
<label for="shipperaddresstwo">Address 2:</label>
<textarea class="form-control" name="shipperaddresstwo" rows="2"></textarea>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-6">
<label for="shipperaddressone">Address:<span>*</span></label>
<textarea class="form-control" name="shipperaddressone" rows="2"></textarea>
 <div class="help-block with-errors"></div>

<label for="shipperaddressthree">Address 3:</label>
<textarea class="form-control" name="shipperaddressthree" rows="2"></textarea>
 <div class="help-block with-errors"></div>
</div>
</fieldset>
<!-- // end shipper -->

<!-- // being consignee -->
 <fieldset class="consignee">
  <legend>Consignee:</legend>
<div class="form-group col-md-6">
<label for="consigneename">Name<span>*</span>:</label>
<input type="text" class="form-control" name="consigneename" id="consigneename" value="">
 <div class="help-block with-errors"></div>
<label for="consigneeaddresstwo">Address 2:</label>
<textarea class="form-control" name="consigneeaddresstwo" rows="2"></textarea>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-6">
<label for="consigneeaddressone">Address<span>*</span>:</label>
<textarea class="form-control " name="consigneeaddressone" rows="2"></textarea>
 <div class="help-block with-errors"></div>

<label for="consigneeaddressthree">Address 3:</label>
<textarea class="form-control" name="consigneeaddressthree" rows="2"></textarea>
</div>
</fieldset>
<!-- // end consignee -->

<!-- // being notify -->
 <fieldset class="notify">
  <legend>Notify:</legend>
<div class="form-group col-md-6">
<label for="notifyname">Name<span>*</span>:</label>
<input type="text" class="form-control input" name="notifyname" id="notifyname" value="">
 <div class="help-block with-errors"></div>
<label for="notifyaddresstwo">Address 2:</label>
<textarea class="form-control" name="notifyaddresstwo" rows="2"></textarea>
</div>
<div class="form-group col-md-6">
<label for="notifyaddressone">Address<span>*</span>:</label>
<textarea class="form-control textarea" name="notifyaddressone" rows="2"></textarea>
<div class="help-block with-errors"></div>

<label for="notifyaddressthree">Address 3:</label>
<textarea class="form-control" name="notifyaddressthree" rows="2"></textarea>
</div>
</fieldset>
<!-- // end notify -->

<!-- // being form detail -->
 <fieldset class="formdetail">
  <legend>Form Detail:</legend>
<div class="form-group col-md-6">
<label for="formeno">Form E No<span>*</span>:</label>
<input type="text" class="form-control input" name="formeno" id="formeno" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-6">
<label for="formedate">Form E Date<span>*</span>:</label>
<input type="text" class="form-control input" name="formedate" id="formedate" value="">
 <div class="help-block with-errors"></div>
</div>
</fieldset>
<!-- // end form detail -->


<!-- // being form Voyage -->
 <fieldset class="voyage">
  <legend>Voyage:</legend>
<div class="form-group col-md-3">
<label for="vessel">Vessel<span>*</span>:</label>
<input type="text" class="form-control input" name="vessel" id="vessel" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-3">
<label for="voyageno"> Voyage No<span>*</span>:</label>
<input type="text" class="form-control input" name="voyageno" id="voyageno" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-3">
<label for="FreightType"> Freight <span>*</span>:</label>
<select name="FreightType" id="FreightType"  class="form-control select">
  <option value="">Freight Type</option>
  <option value="Prepaid At">Prepaid At</option>
  <option value="Collect At">Collect At</option>

</select>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-3">
<label for="other" > &nbsp;</label>
<input type="text" class="form-control" name="other" id="other" value="">
</div>
</fieldset>
<!-- // end form Voyage -->

<!-- // being form location -->
 <fieldset class="location">
  <legend>Location:</legend>
<div class="form-group col-md-4">
<label for="portofloading">Port Of Loading<span>*</span>:</label>
<select name="portofloading" id="portofloading"  class="form-control select">
 <option value="">Non Selected</option>
  <option value="AEAUH">Abu Dhabi</option>
  <option value="AEAJM">AJMAN, UAE</option>
  <option value="BHBAH">BAHRAIN</option>
  <option value="IRBND">BANDAR ABBAS</option>
  <option value="THBKK">BANGKOK</option>
  <option value="CNCAN">BEICUN NANHAI</option>
  <option value="COBOG">BUENAVENTURA</option>
  <option value="KRPUS">BUSAN</option>
  <option value="PEUMO">CALLAO</option>
  <option value="INCOK">COCHIN</option>
  <option value="LKCMB">COLOMBO</option>
  <option value="VNDAD">DA NANG</option>
  <option value="CNDLS">DALIAN</option>
  <option value="SADMN">DAMMAM</option>
  <option value="TZDAR">DAR-ES-SALAM</option>
  <option value="QADOH">DOHA</option>
  <option value="CNFGC">FANGCUN</option>
  <option value="CNFSN">FOSHAN</option>
  <option value="CNFOC">FUZHOU</option>
  <option value="CNGMG">GOAMING</option>
  <option value="VNHPH">HAIPHONG</option>
  <option value="JPHKT">HAKATA, JAPAN</option>
  <option value="CNHSN">HESHAN</option>
  <option value="VNSGN">HOCHIMINH</option>
  <option value="HKHKG">HONG KONG</option>
  <option value="CNHUD">HUADU PORT, CHINA</option>
  <option value="CNHUA">HUANGPU</option>
  <option value="KRINC">INCHEON</option>
  <option value="TRIST">ISTANBUL</option>
  <option value="IDJKT">JAKARTA</option>
  <option value="AEJEA">JEBEL ALI</option>
  <option value="SAJED">Jeddah</option>
  <option value="TWKHH">KAOHSIUNG</option>
  <option value="PKKHI">Karachi</option>
  <option value="TWKEL">KEELUNG</option>
  <option value="JPUKB">KOBE</option>
  <option value="MYBKI">KOTA KINA BALU</option>
  <option value="MYKCH">KUCHING</option>
  <option value="KWKWI">KUWAIT</option>
  <option value="KRKWY">KWANGYANG</option>
  <option value="THLCH">LAEM CHABANG</option>
  <option value="CNLSI">LANSHI</option>
  <option value="THLKA">LAT KRABANG</option>
  <option value="CNPLW">LELIU , SHUNDE</option>
  <option value="CNLYG">LIANYUNGANG</option>
  <option value="USLGB">LONG BEACH, USA</option>
  <option value="USLAX">LOS ANGELES, USA</option>
  <option value="PHMNN">MANILA, NORTH PORT</option>
  <option value="MXMEX">MANZANILLO</option>
  <option value="JPMIZ">MIZUSHIMA</option>
  <option value="JPMOJ">MOJI, JAPAN</option>
  <option value="KEMBA">MOMBASA</option>
  <option value="INMUN">MUNDRA</option>
  <option value="OMMCT">MUSCAT</option>
  <option value="JPNGO">NAGOYA</option>
  <option value="JPNAH">NAHA OKINAWA</option>
  <option value="CNNKG">NANJING, CHINA</option>
  <option value="CNNSA">NANSHA</option>
  <option value="CNNTG">NANTONG</option>
  <option value="CNNBO">NINGBO</option>
  <option value="USOAK">OAKLAND</option>
  <option value="JPOSA">OSAKA, JAPAN</option>
  <option value="MYPGD">PASIR GUDANG</option>
  <option value="MYPEN">Penang</option>
  <option value="PHMNS">PHMNN</option>
  <option value="MYPKG">PORT KELANG</option>
  <option value="AERSH">PORT RASHID</option>
  <option value="CNTAO">QINGDAO</option>
  <option value="SARUH">RIYADH</option>
  <option value="CNSNB">SANBU</option>
  <option value="MYSDK">SANDAKHAN</option>
  <option value="CNSSI">SANSHUI</option>
  <option value="CNSHA">SHANGHAI</option>
  <option value="CNSSN">SHANSHAN NANHAI</option>
  <option value="CNSWA">SHANTOU</option>
  <option value="AESHJ">SHARJAH</option>
  <option value="CNSTN">SHATIAN</option>
  <option value="CNSKU">SHEKOU</option>
  <option value="JPSMZ">SHIMIZU</option>
  <option value="KWSAA">SHUAIBA</option>
  <option value="KWSWK">SHUWAIKH</option>
  <option value="MYSBW">SIBU</option>
  <option value="SGSIN">SINGAPORE</option>
  <option value="OMSOH">SOHAR/MUSCAT</option>
  <option value="MNL">SOUTH HARBOR</option>
  <option value="IDSUB">SURABAYA</option>
  <option value="AUSYD">SYDNEY PORT</option>
  <option value="TWTXG">TAICHUNG</option>
  <option value="TNTPE">TAOYOAN</option>
  <option value="NYTWU">TAWAU</option>
  <option value="CNSHK">TIANJIN</option>
  <option value="JPTYO">TOKYO</option>
  <option value="INTUT">TUTICORIN</option>
  <option value="IQUQR">UMM E QASR          </option>
  <option value="VNVCT">VICT HOCHIMINH </option>
  <option value="CNWHI">WUHU, CHINA</option>
  <option value="CNXMN">XIAMEN</option>
  <option value="CNHSK">XINGANG</option>
  <option value="CNYTN">YANTIAN, CHINA</option>
  <option value="JPYOK">YOKOHAMA</option>
  <option value="CNZTG">ZHANJIAGANG</option>
  <option value="CNZSN">ZHONGSHAN CHINA</option>
  <option value="CNZUH">ZHUHAI, CHINA</option>

</select>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="portofdischarge"> Port of Discharge<span>*</span>:</label>
<select name="portofdischarge" id="portofdischarge"  class="form-control select">
  <option value="">Non Selected</option>
  <option value="AEAUH">Abu Dhabi</option>
  <option value="AEAJM">AJMAN, UAE</option>
  <option value="BHBAH">BAHRAIN</option>
  <option value="IRBND">BANDAR ABBAS</option>
  <option value="THBKK">BANGKOK</option>
  <option value="CNCAN">BEICUN NANHAI</option>
  <option value="COBOG">BUENAVENTURA</option>
  <option value="KRPUS">BUSAN</option>
  <option value="PEUMO">CALLAO</option>
  <option value="INCOK">COCHIN</option>
  <option value="LKCMB">COLOMBO</option>
  <option value="VNDAD">DA NANG</option>
  <option value="CNDLS">DALIAN</option>
  <option value="SADMN">DAMMAM</option>
  <option value="TZDAR">DAR-ES-SALAM</option>
  <option value="QADOH">DOHA</option>
  <option value="CNFGC">FANGCUN</option>
  <option value="CNFSN">FOSHAN</option>
  <option value="CNFOC">FUZHOU</option>
  <option value="CNGMG">GOAMING</option>
  <option value="VNHPH">HAIPHONG</option>
  <option value="JPHKT">HAKATA, JAPAN</option>
  <option value="CNHSN">HESHAN</option>
  <option value="VNSGN">HOCHIMINH</option>
  <option value="HKHKG">HONG KONG</option>
  <option value="CNHUD">HUADU PORT, CHINA</option>
  <option value="CNHUA">HUANGPU</option>
  <option value="KRINC">INCHEON</option>
  <option value="TRIST">ISTANBUL</option>
  <option value="IDJKT">JAKARTA</option>
  <option value="AEJEA">JEBEL ALI</option>
  <option value="SAJED">Jeddah</option>
  <option value="TWKHH">KAOHSIUNG</option>
  <option value="PKKHI">Karachi</option>
  <option value="TWKEL">KEELUNG</option>
  <option value="JPUKB">KOBE</option>
  <option value="MYBKI">KOTA KINA BALU</option>
  <option value="MYKCH">KUCHING</option>
  <option value="KWKWI">KUWAIT</option>
  <option value="KRKWY">KWANGYANG</option>
  <option value="THLCH">LAEM CHABANG</option>
  <option value="CNLSI">LANSHI</option>
  <option value="THLKA">LAT KRABANG</option>
  <option value="CNPLW">LELIU , SHUNDE</option>
  <option value="CNLYG">LIANYUNGANG</option>
  <option value="USLGB">LONG BEACH, USA</option>
  <option value="USLAX">LOS ANGELES, USA</option>
  <option value="PHMNN">MANILA, NORTH PORT</option>
  <option value="MXMEX">MANZANILLO</option>
  <option value="JPMIZ">MIZUSHIMA</option>
  <option value="JPMOJ">MOJI, JAPAN</option>
  <option value="KEMBA">MOMBASA</option>
  <option value="INMUN">MUNDRA</option>
  <option value="OMMCT">MUSCAT</option>
  <option value="JPNGO">NAGOYA</option>
  <option value="JPNAH">NAHA OKINAWA</option>
  <option value="CNNKG">NANJING, CHINA</option>
  <option value="CNNSA">NANSHA</option>
  <option value="CNNTG">NANTONG</option>
  <option value="CNNBO">NINGBO</option>
  <option value="USOAK">OAKLAND</option>
  <option value="JPOSA">OSAKA, JAPAN</option>
  <option value="MYPGD">PASIR GUDANG</option>
  <option value="MYPEN">Penang</option>
  <option value="PHMNS">PHMNN</option>
  <option value="MYPKG">PORT KELANG</option>
  <option value="AERSH">PORT RASHID</option>
  <option value="CNTAO">QINGDAO</option>
  <option value="SARUH">RIYADH</option>
  <option value="CNSNB">SANBU</option>
  <option value="MYSDK">SANDAKHAN</option>
  <option value="CNSSI">SANSHUI</option>
  <option value="CNSHA">SHANGHAI</option>
  <option value="CNSSN">SHANSHAN NANHAI</option>
  <option value="CNSWA">SHANTOU</option>
  <option value="AESHJ">SHARJAH</option>
  <option value="CNSTN">SHATIAN</option>
  <option value="CNSKU">SHEKOU</option>
  <option value="JPSMZ">SHIMIZU</option>
  <option value="KWSAA">SHUAIBA</option>
  <option value="KWSWK">SHUWAIKH</option>
  <option value="MYSBW">SIBU</option>
  <option value="SGSIN">SINGAPORE</option>
  <option value="OMSOH">SOHAR/MUSCAT</option>
  <option value="MNL">SOUTH HARBOR</option>
  <option value="IDSUB">SURABAYA</option>
  <option value="AUSYD">SYDNEY PORT</option>
  <option value="TWTXG">TAICHUNG</option>
  <option value="TNTPE">TAOYOAN</option>
  <option value="NYTWU">TAWAU</option>
  <option value="CNSHK">TIANJIN</option>
  <option value="JPTYO">TOKYO</option>
  <option value="INTUT">TUTICORIN</option>
  <option value="IQUQR">UMM E QASR          </option>
  <option value="VNVCT">VICT HOCHIMINH </option>
  <option value="CNWHI">WUHU, CHINA</option>
  <option value="CNXMN">XIAMEN</option>
  <option value="CNHSK">XINGANG</option>
  <option value="CNYTN">YANTIAN, CHINA</option>
  <option value="JPYOK">YOKOHAMA</option>
  <option value="CNZTG">ZHANJIAGANG</option>
  <option value="CNZSN">ZHONGSHAN CHINA</option>
  <option value="CNZUH">ZHUHAI, CHINA</option>

</select>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="placeofdelivery"> Place of Delivery<span>*</span>:</label>
<select name="placeofdelivery" id="placeofdelivery"  class="form-control select" >
 <option >Non Selected</option>
  <option value="AEAUH">Abu Dhabi</option>
  <option value="AEAJM">AJMAN, UAE</option>
  <option value="BHBAH">BAHRAIN</option>
  <option value="IRBND">BANDAR ABBAS</option>
  <option value="THBKK">BANGKOK</option>
  <option value="CNCAN">BEICUN NANHAI</option>
  <option value="COBOG">BUENAVENTURA</option>
  <option value="KRPUS">BUSAN</option>
  <option value="PEUMO">CALLAO</option>
  <option value="INCOK">COCHIN</option>
  <option value="LKCMB">COLOMBO</option>
  <option value="VNDAD">DA NANG</option>
  <option value="CNDLS">DALIAN</option>
  <option value="SADMN">DAMMAM</option>
  <option value="TZDAR">DAR-ES-SALAM</option>
  <option value="QADOH">DOHA</option>
  <option value="CNFGC">FANGCUN</option>
  <option value="CNFSN">FOSHAN</option>
  <option value="CNFOC">FUZHOU</option>
  <option value="CNGMG">GOAMING</option>
  <option value="VNHPH">HAIPHONG</option>
  <option value="JPHKT">HAKATA, JAPAN</option>
  <option value="CNHSN">HESHAN</option>
  <option value="VNSGN">HOCHIMINH</option>
  <option value="HKHKG">HONG KONG</option>
  <option value="CNHUD">HUADU PORT, CHINA</option>
  <option value="CNHUA">HUANGPU</option>
  <option value="KRINC">INCHEON</option>
  <option value="TRIST">ISTANBUL</option>
  <option value="IDJKT">JAKARTA</option>
  <option value="AEJEA">JEBEL ALI</option>
  <option value="SAJED">Jeddah</option>
  <option value="TWKHH">KAOHSIUNG</option>
  <option value="PKKHI">Karachi</option>
  <option value="TWKEL">KEELUNG</option>
  <option value="JPUKB">KOBE</option>
  <option value="MYBKI">KOTA KINA BALU</option>
  <option value="MYKCH">KUCHING</option>
  <option value="KWKWI">KUWAIT</option>
  <option value="KRKWY">KWANGYANG</option>
  <option value="THLCH">LAEM CHABANG</option>
  <option value="CNLSI">LANSHI</option>
  <option value="THLKA">LAT KRABANG</option>
  <option value="CNPLW">LELIU , SHUNDE</option>
  <option value="CNLYG">LIANYUNGANG</option>
  <option value="USLGB">LONG BEACH, USA</option>
  <option value="USLAX">LOS ANGELES, USA</option>
  <option value="PHMNN">MANILA, NORTH PORT</option>
  <option value="MXMEX">MANZANILLO</option>
  <option value="JPMIZ">MIZUSHIMA</option>
  <option value="JPMOJ">MOJI, JAPAN</option>
  <option value="KEMBA">MOMBASA</option>
  <option value="INMUN">MUNDRA</option>
  <option value="OMMCT">MUSCAT</option>
  <option value="JPNGO">NAGOYA</option>
  <option value="JPNAH">NAHA OKINAWA</option>
  <option value="CNNKG">NANJING, CHINA</option>
  <option value="CNNSA">NANSHA</option>
  <option value="CNNTG">NANTONG</option>
  <option value="CNNBO">NINGBO</option>
  <option value="USOAK">OAKLAND</option>
  <option value="JPOSA">OSAKA, JAPAN</option>
  <option value="MYPGD">PASIR GUDANG</option>
  <option value="MYPEN">Penang</option>
  <option value="PHMNS">PHMNN</option>
  <option value="MYPKG">PORT KELANG</option>
  <option value="AERSH">PORT RASHID</option>
  <option value="CNTAO">QINGDAO</option>
  <option value="SARUH">RIYADH</option>
  <option value="CNSNB">SANBU</option>
  <option value="MYSDK">SANDAKHAN</option>
  <option value="CNSSI">SANSHUI</option>
  <option value="CNSHA">SHANGHAI</option>
  <option value="CNSSN">SHANSHAN NANHAI</option>
  <option value="CNSWA">SHANTOU</option>
  <option value="AESHJ">SHARJAH</option>
  <option value="CNSTN">SHATIAN</option>
  <option value="CNSKU">SHEKOU</option>
  <option value="JPSMZ">SHIMIZU</option>
  <option value="KWSAA">SHUAIBA</option>
  <option value="KWSWK">SHUWAIKH</option>
  <option value="MYSBW">SIBU</option>
  <option value="SGSIN">SINGAPORE</option>
  <option value="OMSOH">SOHAR/MUSCAT</option>
  <option value="MNL">SOUTH HARBOR</option>
  <option value="IDSUB">SURABAYA</option>
  <option value="AUSYD">SYDNEY PORT</option>
  <option value="TWTXG">TAICHUNG</option>
  <option value="TNTPE">TAOYOAN</option>
  <option value="NYTWU">TAWAU</option>
  <option value="CNSHK">TIANJIN</option>
  <option value="JPTYO">TOKYO</option>
  <option value="INTUT">TUTICORIN</option>
  <option value="IQUQR">UMM E QASR          </option>
  <option value="VNVCT">VICT HOCHIMINH </option>
  <option value="CNWHI">WUHU, CHINA</option>
  <option value="CNXMN">XIAMEN</option>
  <option value="CNHSK">XINGANG</option>
  <option value="CNYTN">YANTIAN, CHINA</option>
  <option value="JPYOK">YOKOHAMA</option>
  <option value="CNZTG">ZHANJIAGANG</option>
  <option value="CNZSN">ZHONGSHAN CHINA</option>
  <option value="CNZUH">ZHUHAI, CHINA</option>

</select>
 <div class="help-block with-errors"></div>
</div>

</fieldset>
<!-- // end form location -->



<!-- // being form Number Of Original BL -->
 <fieldset class="originalbl">
  <legend>Number Of Original BL:</legend>
<div class="form-group col-md-4">
<label for="blnumber">BL Number<span>*</span>:</label>
<select name="blnumber" id="blnumber"  class="form-control select">
  <option value="">BL Number</option>
  <option value="1">1</option>
  <option value="2">2</option>
   <option value="3">3</option>

</select>
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="commodity"> Commodity<span>*</span>:</label>
<input type="text" class="form-control input" name="commodity" id="commodity" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="packagestype"> Packages Type<span>*</span>:</label>
<select name="packagestype" id="packagestype"  class="form-control select">
 <option value="">Non Selected</option>
  <option value="BAG">BAG</option>
  <option value="BAG     ">BAGS                </option>
  <option value="BLE     ">BALE                </option>
  <option value="BLE     ">BALES               </option>
  <option value="BAR     ">BARE                </option>
  <option value="BLL     ">BARREL              </option>
  <option value="BSK     ">BASKET              </option>
  <option value="BLK     ">BLOCK               </option>
  <option value="BOB     ">BOBBIN              </option>
  <option value="BOR     ">BORAS               </option>
  <option value="BOT     ">BOTTLE              </option>
  <option value="BOX     ">BOXES               </option>
  <option value="BKT     ">BUCKET              </option>
  <option value="BUL     ">BULK                </option>
  <option value="BUH     ">BUNCH               </option>
  <option value="BUND    ">BUND                </option>
  <option value="BDL     ">BUNDLES             </option>
  <option value="CAN     ">CANS                </option>
  <option value="CTN     ">CARTONS             </option>
  <option value="CAS     ">CASES               </option>
  <option value="CBM     ">CBM                 </option>
  <option value="CHS     ">CHEST               </option>
  <option value="COL     ">COILS               </option>
  <option value="CBC     ">CONTAINER BUL CARGO </option>
  <option value="CNT     ">CONTAINERS          </option>
  <option value="CRT     ">CRATES              </option>
  <option value="CYL     ">CYLINDERS           </option>
  <option value="DZN     ">DOZEN               </option>
  <option value="DEN     ">DOZENS              </option>
  <option value="DRU     ">DRUMS               </option>
  <option value="DBK     ">DRY BULK            </option>
  <option value="ENV     ">ENVELOPES           </option>
  <option value="FRM     ">FRAME               </option>
  <option value="ITE     ">ITEMS               </option>
  <option value="KGS     ">KILOGRAMS           </option>
  <option value="LOT     ">LOT                 </option>
  <option value="MBAGS   ">M/JUTE BAGS         </option>
  <option value="MARBLE  ">MARBLE              </option>
  <option value="NUM     ">NUMBERS             </option>
  <option value="PKG     ">PACKAGE             </option>
  <option value="PKG     ">PACKAGES            </option>
  <option value="PKT     ">PACKET              </option>
  <option value="PAL     ">PAILS               </option>
  <option value="PLT     ">PALLETS             </option>
  <option value="BAG     ">PAPER BAGS          </option>
  <option value="PCE     ">PCE                 </option>
  <option value="PCE     ">PIECES              </option>
  <option value="POL     ">POLES               </option>
  <option value="PLC     ">POLYCONES           </option>
  <option value="REL     ">REELS               </option>
  <option value="REM     ">REM                 </option>
  <option value="ROL     ">ROLLS               </option>
  <option value="SAK     ">SACK                </option>
  <option value="SPE     ">SCRAPE              </option>
  <option value="SET     ">SETS                </option>
  <option value="SHE     ">SHEETS              </option>
  <option value="SKD     ">SKIDS               </option>
  <option value="TIN     ">TINS                </option>
  <option value="TNK     ">TNK                 </option>
  <option value="UNT     ">UNITS               </option>
  <option value="VAN     ">VAN                 </option>
  <option value="WS      ">WHEAT STRAW         </option>
  <option value="W/B     ">WOODEN BOXES        </option>
  <option value="WDC     ">WOODEN CASES        </option>
  <option value="YRD     ">YARDS               </option>

</select>
 <div class="help-block with-errors"></div>
</div>

</fieldset>
<!-- // end form Number Of Original BL -->


<!-- // being Container -->
 <fieldset class="containers">
  <legend>Containers:</legend>
<div class="col-md-6" id="tab_containers">

<div class="row-add-containers" >
<div class="row">
  <div class="form-group col-md-4">
<label for="containernumber"> Container Number<span>*</span>:</label>
<input type="text" class="form-control input" name="containernumber[]" id="containernumber" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="sealnumber"> Seal Number<span>*</span>:</label>
<input type="text" class="form-control input" name="sealnumber[]" id="sealnumber" value="">
 <div class="help-block with-errors"></div>
</div>
<div class="form-group col-md-4">
<label for="noofpackages"> No Of Packages<span>*</span>:</label>
<input type="text" class="form-control input" name="noofpackages[]" id="noofpackages" value="">
 <div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-12"></div>
</div>
<a id="add_containers" class="btn btn-default">Add Container</a>

</div>

<div class=" col-md-6" id="tab_mark">

<div class="row">
  <div class="form-group col-md-12" >
    
 <div class="row-add-mark-no">
<label for="marksno">Marks &amp; No.<span>*</span>:</label>
<input type="text" class="form-control input" name="marksno[]" id="marksno" value="">
 <div class="help-block with-errors"></div>
</div>
<a id="add_mark_no" class="btn btn-default">Add Mark &amp; No</a>
</div>
</div>

</div>

</fieldset>
<!-- // end Container -->



<!-- // being Description Of Goods -->
 <fieldset class="descriptionGoods">
  <legend>Description Of Goods:</legend>
<div class="form-group col-md-12">
<div class="row-add-description">
  <label for="description" > &nbsp;</label>
<textarea class="form-control" name="description[]" rows="2"></textarea>
</div>
<a id="add_description" class="btn btn-default">Add Description</a>
</div>
</fieldset>
<!-- // end Description Of Goods -->

<!-- // being Weight -->
 <fieldset class="weight">
  <legend>Weight:</legend>
<div class="form-group col-md-4">
<label for="netweight" > Net Weight</label>
<input type="text" class="form-control" name="netweight" id="netweight" value="">
<span> kg</span>
</div>
<div class="form-group col-md-4">
<label for="grossweight" > Gross Weigh</label>
<input type="text" class="form-control" name="grossweight" id="grossweight" value="">
<span> kg</span>
</div>
<div class="form-group col-md-4">
<label for="measurement" > Measurement</label>
<input type="text" class="form-control" name="measurement" id="measurement" value="">
</div>
</fieldset>
<!-- // end Weight -->



<div class="row text-center">
  
  <div class="form-group">
<input type="submit" value=" Preview BL" name="previewbl" class="btn btn-success">
</div>
</div>
</form>
		
	</div>

                </div>
            </div>
		</div>
		

	
<script>
  
/*   $('.form').attr('data-toggle="validator"');
       //$("input,textarea,select").prop('required',true);
        $(".req").prop('required',true);
       $('.form').validator();*/
  $("#add_containers").click(function(){addnewcontainers();});     
 $("#add_mark_no").click(function(){addnewmarkno();});
 $("#add_description").click(function(){addnewdescription();});
 function addnewcontainers(){
    var cont = ($('.row-add-containers') .length-0)+1;
    var tr = '<div class="row">'+
        '<div class="col-md-4"><input type="text" class="form-control input" name="containernumber[]" id="containernumber_'+cont+'" value=""></div>'+
        '<div class="col-md-4"><input type="text" class="form-control input" name="sealnumber[]" id="sealnumber_'+cont+'" value=""></div>'+
        '<div class="col-md-4"><input type="text" class="form-control input" name="noofpackages[]" id="noofpackages_'+cont+'" value=""></div>'+'</div>';
    $(".row-add-containers").append(tr);
  }
   function addnewmarkno(){
    var markno = ($('.row-add-mark-no') .length-0)+1;
    var tr = '<input type="text" class="form-control input" name="marksno[]" id="marksno_'+markno+'" value="">';
    $(".row-add-mark-no").append(tr);
  }    
 
   function addnewdescription(){
    var desc = ($('.row-add-description') .length-0)+1;
    var tr = '<textarea class="form-control" name="description[]" id="description_'+desc+'" rows="2"></textarea>';
    $(".row-add-description").append(tr);
  }
 
  $('body').delegate('#remove-containers','click',function(){
        $(this).parent().parent().remove();
      });
    $('body').delegate('#remove-mark-no','click',function(){
        $(this).parent().parent().remove();
      });
      $('body').delegate('#remove-description','click',function(){
        $(this).parent().parent().remove();
      });
</script>
<?php require_once 'footer.php'; ?>
	
</body>
</html>