


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Riazeda Pvt Ltd - View Bill Of Lading
</title>
    <style type="text/css">
        table
        {
            border-collapse: collapse;
            border-color: Gray;
        }
        td
        {
            border-color: Gray;
            margin-left: 80px;
        }
        .text1
        {
            font: 9pt verdana, arial, sans-serif;
        }
        .text1label
        {
            font: bold 9pt/12pt verdana, arial, sans-serif;
        }
        .aligncenter
        {
            text-align: center;
        }
        .style1
        {
            height: 93px;
        }
        .style2
        {
            width: 382px;
        }
        .style3
        {
            width: 40%;
        }
    </style>
</head>
<body>
    <form method="post" action="ViewBL.aspx?ID=14961" id="form1">
<div class="aspNetHidden">
<input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="" />
</div>

<div class="aspNetHidden">

	<input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="866F628A" />
	<input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="/wEdAARpDekW6F3Xi/Kh4o23DA0yESCFkFW/RuhzY1oLb/NUVDzmltaUM7aEAN+g9cP/m13ZfcV5SvQ5Vasn18Sy6zMnBR4OlJFTLs5PKJfF5vi7ySZZPEk=" />
</div>
    <div align="center" style="font-weight: bold; font-size: 20px;">
        Riazeda (Pvt) Limited
    </div>
    <div align="center">
        
        <table cellpadding='0' cellspacing='0' width='100%' border="1">
            <tr>
                <td colspan='2' align="left" style="height: 15px">
                </td>
            </tr>
            <tr>
                <td align="left" valign="top" style="width: 50%;">
                    <table width="100%" border="1">
                        
                        <tr>
                            <td valign="top" align="left" colspan="2">
                                <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td>
                                            <span class="text1label">Shipper : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <span class="text1">
                                                <span id="ShipperNameLabel">TANISHA LANDRY</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="ShipperAddress1Label">21 CLARENDON COURT</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="ShipperAddress2Label">TEMPORA CUPIDATAT FACERE VOLUPTATEM VOLUPTATEM LABORIOSAM</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="ShipperAddress3Label">EXPLICABO FUGIAT UT ID MOLESTIAS ALIQUAM</span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top" align="left" class="style1">
                                <table>
                                    <tr>
                                        <td class="style2">
                                            <table border="0" cellspacing="0" cellpadding="2">
                                                <tr>
                                                    <td>
                                                        <span class="text1label">Consignee : </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top">
                                                        <span class="text1">
                                                            <span id="ConsigneeNameLabel">PIPER RIVAS</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="ConsigneeAddress1Label">95 NOBEL DRIVE</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="ConsigneeAddress2Label">QUOD ET DOLOR ALIQUIP QUOD MOLESTIAS PARIATUR NULLA VOLUPTA</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="ConsigneeAddress3Label">SED QUIA EA DOLOREM QUIA EXPEDITA UT MOLLITIA ASPERIORES NAM</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="style2">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <span class="text1label">eForm No : </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="eFormNoLabel">QUIBUSDAM ID QUO VOL</span>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1label">eForm Date : </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="eFormDateLabel">25 Jun 2005</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top" align="left">
                                <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td>
                                            <span class="text1label">Notify Party : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <span class="text1">
                                                <span id="NotifyNameLabel">MOLESTIAE ET IRURE SED LAUDANTIUM UT LABORUM BEAT</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="NotifyAddress1Label">36 OLD AVENUE</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="NotifyAddress2Label">NON PARIATUR IPSA NULLA TEMPORIBUS OMNIS ENIM</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="NotifyAddress3Label">TOTAM CUMQUE AUT CULPA VITAE NISI CONSEQUATUR CUMQUE NEQUE </span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top">
                                <table border="0" cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td class="style3">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <span class="text1label">Vessel : </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1label">Voyage : </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1label">Port of Load : </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="VesselLabel">QUOD QUAS CORRUPTI </span></span>
                                                    </td>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="VoyageLabel">SINT </span>
                                                        </span>
                                                    </td>
                                                    <td>
                                                        <span class="text1">
                                                            <span id="POLLabel">CALLAO</span>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left" valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="text1label">Port of Discharge : </span>
                                        </td>
                                        <td>
                                            <span class="text1label">Place of Delivery : </span>
                                        </td>
                                        <td>
                                            <span class="text1label">Type of Packages : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1">
                                                <span id="PODLabel">DOHA</span>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text1">
                                                <span id="PODelLabel">ZHONGSHAN CHINA</span>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text1">
                                                <span id="PackageTypeLabel">DBK     </span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </td>
                <td align="left" valign="top" style="width: 50%;">
                    <img id="BLImage" src="wanhaiBL.jpg" />
                </td>
            </tr>
            <tr>
                <td colspan='2' align="left" valign="top">
                    <table cellpadding="2" cellspacing="2">
                        <tr>
                            <td align="left" valign="top" style="margin: 0px 10px 0px 10px; width: 50%">
                                <div>
	<table cellspacing="0" cellpadding="5" id="GridView1" style="border-collapse:collapse;">
		<tr>
			<th scope="col">ContainerNumber</th><th scope="col">SealNo</th><th scope="col">Packages</th>
		</tr><tr>
			<td>123</td><td>123</td><td>12</td>
		</tr>
	</table>
</div>
                            </td>
                            <td align="left" valign="top" style="margin: 0px 10px 0px 10px; width: 30%">
                                <div>
	<table cellspacing="0" cellpadding="5" id="DescriptionGrid" style="border-collapse:collapse;">
		<tr>
			<th scope="col">Description</th>
		</tr><tr>
			<td>VOLUPTAS OFFICIA DUIS VOLUPTAT</td>
		</tr>
	</table>
</div>
                            </td>
                            <td align="left" valign="top" style="margin: 0px 10px 0px 10px; width: 20%">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="text1label">Net Weight : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1 aligncenter">
                                                <span id="NetWeightLabel">56 KGs</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1label">Gross Weight : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1 aligncenter">
                                                <span id="GrossWeightLabel">12 KGs</span>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1label">Measure : </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="text1 aligncenter">
                                                <span id="MeasureLabel">65</span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan='2' align="left">
                    <table border='0' cellspacing="50" cellpadding="3">
                        <tr>
                            <td valign="top">
                                <div style="margin: 0px 100px 0px 0px;">
                                    <div>
	<table cellspacing="0" cellpadding="5" id="MarksGrid" style="border-collapse:collapse;">
		<tr>
			<th scope="col">MarksAndNumber</th>
		</tr><tr>
			<td>REICIENDIS QUIDEM QUIDEM VELIT</td>
		</tr>
	</table>
</div>
                                </div>
                            </td>
                            <td valign="bottom">
                                <table>
                                    <tr>
                                        <td>
                                            <span class="text1label">No. Of B(s)/L : </span>
                                        </td>
                                        <td>
                                            <span class="text1 aligncenter">
                                                <span id="NoOfBLLabel">2</span>
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text1label">Freight : </span>
                                        </td>
                                        <td>
                                            <span class="text1 aligncenter">
                                                <span id="FreightLabel">Collect At NIHIL OFFICIIS PROIDENT FACERE ENIM</span>
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td valign="top" align="left" colspan="2">
                    <span class="text1 aligncenter">
                        <span id="BLSubmitedDateLabel">BL submitted on 1/6/2018 11:35:41 AM</span>
                    </span>
                </td>
            </tr>
        </table>
             <span id="Label1">Please Enter Email ID for Checking copy</span>
              <input name="TextBox1" type="text" id="TextBox1" style="width:363px;" />
        <input type="submit" name="btnSubmit" value="Submit" id="btnSubmit" />
        &nbsp;<input type="submit" name="btnEdit" value="Edit BL" id="btnEdit" />
        <br />
    </div>
    </form>
</body>
</html>