<?php
if(is_file("../include/inc_head.php")){
require_once("../include/inc_head.php");
}else{
  echo "please check the configure files"; 
}
if(isset($_POST['login'])){
$username = db_escape($db,$_POST['username']);
$password = db_escape($db,md5($_POST['password']));

$sql = "SELECT id,username,hostname,password,status FROM registered WHERE username = '$username' AND password = '$password'";
$run_query = mysqli_query($db,$sql);
$count = mysqli_num_rows($run_query);
 if($count == 1){
  $row = mysqli_fetch_array($run_query);
  $_SESSION['clogged_in'] = true;
  $_SESSION['cid'] = $row['id'];
  $_SESSION['cusername'] = $row['username'];
  $_SESSION['chostname'] = $row['hostname'];
  if($row['status'] == 'active'){ 
 /*$loginattemptsSql = "SELECT id,ip,attempts,username,DATE_SUB('last_login', INTERVAL -1 HOUR) as last_login FROM loginattempts WHERE username = '$username' ";
  $loginattemptsQuery = mysqli_query($db,$loginattemptsSql);
  $loginattemptsCount = mysqli_num_rows($loginattemptsQuery);
   $loginattemptsRo= mysqli_fetch_assoc($loginattemptsQuery);
   $loginattemptsRow=$loginattemptsRo['last_login'];
   $lockout_time = date("Y-m-d H:i:s");
   $last_login = strtotime($loginattemptsRow['last_login']) + 60*60;
   $last_login = date("Y-m-d H:i:s", $last_login);
   echo '<br/>'.$loginattemptsCount."<br>".$lockout_time;
   die();
  if( $loginattemptsCount == 3  ){
    
     if($last_login<$lockout_time){     
    $msg = '<div class="text-danger alert-dismissable">User Disabled</div>';
  }
  }else{
  }*/
 
  redirect(BASE_URL.'/clients/export.php');



  }else{
   $msg =  '<div class="text-danger alert-dismissable">User not Inactive</div>';
  }
 }else{
  // Insert Query
  /*$ip = get_ip_address();
  $loginattemptsQ = "INSERT INTO `loginattempts` (ip,attempts,username) VALUES ('$ip','1','$username')";
  $loginattempts_query = mysqli_query($db,$loginattemptsQ);*/
 $msg = '<div class="text-danger alert-dismissable">Invalid credentials</div>';
 }

}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login Form</title>
	<?php require_once '../include/inc_css_base.php'; ?>

  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/include/css/login.css">

	  <style>

  </style>
</head>
<body>


  <section id="login">
    <div class="container">
      <div class="row">
          <div class="col-xs-12">
              <div class="form-wrap">
                <?php if(isset($msg)){ echo $msg; } ?>
                <h1>Log in with your User account</h1>
                    <form role="form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" id="login-form" autocomplete="off">
                        <div class="form-group">
                            <label for="email" class="sr-only">Username</label>
                            <input type="text" name="username" id="username" class="form-control input-sm" placeholder="Username" tabindex="1">
                        </div>
                        <div class="form-group">
                            <label for="key" class="sr-only">Password</label>
                           <!--  <input type="password" name="key" id="key" class="form-control" placeholder="Password"> -->
                             <input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password" tabindex="2">
                        </div>
                        
                        <input type="submit" id="btn-login"  name="login" class="btn btn-custom btn-block" value="Log in"  tabindex="3">
                        
                    </form>
                    <a href="" class="forget" data-toggle="modal" data-target=".forget-modal">Forgot your password?</a>
                   
              </div>
        </div> <!-- /.col-xs-12 -->
      </div> <!-- /.row -->
    </div> <!-- /.container -->
</section>
<div class="modal fade forget-modal" tabindex="-1" role="dialog" aria-labelledby="myForgetModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">
          <span aria-hidden="true">×</span>
          <span class="sr-only">Close</span>
        </button>
        <h4 class="modal-title">Recovery password</h4>
      </div>
      <div class="modal-body">
        <p>Type your email account</p>
        <input type="email" name="recovery-email" id="recovery-email" class="form-control" autocomplete="off">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-custom">Recovery</button>
      </div>
    </div> <!-- /.modal-content -->
  </div> <!-- /.modal-dialog -->
</div> <!-- /.modal -->

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                 <?php echo $_SITE['copyright']; ?>
            </div>
        </div>
    </div>
</footer>




 <?php require_once '../include/inc_js_base_client.php';?> 

</body>
</html>