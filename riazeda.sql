-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 29, 2017 at 05:09 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riazeda`
--

-- --------------------------------------------------------

--
-- Table structure for table `defaultpage`
--

CREATE TABLE IF NOT EXISTS `defaultpage` (
  `default_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `default_value` varchar(30) NOT NULL,
  `website_type` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `defaultpage`
--

INSERT INTO `defaultpage` (`default_id`, `post_id`, `default_value`, `website_type`) VALUES
(1, 12, 'about', 1),
(2, 10, 'Home', 3);

-- --------------------------------------------------------

--
-- Table structure for table `postmeta`
--

CREATE TABLE IF NOT EXISTS `postmeta` (
  `meta_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postmeta`
--

INSERT INTO `postmeta` (`meta_id`, `post_id`, `meta_value`) VALUES
(1, 30, 'page-home.php'),
(2, 31, 'page-about.php'),
(8, 2, 'page-about.php');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `images` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_updated` datetime DEFAULT NULL,
  `website_type` int(11) NOT NULL,
  `post_type` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COMMENT='posts (type mention in posts or page website) or website_type: (mention in 3 difference  website )';

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `body`, `images`, `userId`, `created_at`, `created_updated`, `website_type`, `post_type`, `status`) VALUES
(1, 'Hello World', 'hello-world', 'The quick brown fox jumped over the lazy dog.', '', 1, '2017-09-23 12:20:39', '2017-09-24 00:00:00', 1, 'posts', 1),
(2, 'Sample Pages', 'sample-pages', '<p>asasasasas</p>', 'upload/fc738845e767a0717628ac2752d001e0.png', 1, '2017-09-23 12:29:42', NULL, 3, 'pages', 1),
(5, 'post 1', 'post-1', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse feugiat at neque vitae eleifend. Donec tristique erat ut leo vehicula, in ornare diam ornare. Duis mattis bibendum arcu, ut sollicitudin mauris tincidunt vel. Quisque auctor vulputate sagittis. In sit amet neque ac mauris finibus tempus. Suspendisse id sagittis massa. Integer vestibulum quam sed felis rutrum maximus. Aliquam id tellus velit. Vestibulum sit amet pellentesque enim, ut rutrum tellus. Ut tempor tempus odio, a dictum odio vulputate in. Suspendisse iaculis nunc id vestibulum ornare. Duis eget fringilla mi.</p>', 'upload/fc738845e767a0717628ac2752d001e0.png', 1, '2017-09-24 07:14:25', NULL, 2, 'posts', 1),
(6, 'post 2', 'post-2', '<p>Mauris hendrerit facilisis sem eget posuere. Duis viverra vitae tellus eget aliquet. Duis elementum interdum velit vitae tempus. Sed tempus dolor magna, ut feugiat nisi commodo eu. Nunc ut metus neque. Curabitur at finibus est, in laoreet ex. Curabitur vel orci ac nisl bibendum dictum. Quisque non volutpat nunc, non placerat purus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Morbi tristique rutrum elit eu viverra.</p>', 'upload/36f98b3fda50f976f195c138399ace59.png', 1, '2017-09-24 07:14:59', NULL, 2, 'posts', 1),
(7, 'post 3', 'post-3', '<p>Proin scelerisque ullamcorper libero, et semper lacus congue a. Donec malesuada nibh eget dolor tincidunt, ut ultricies nunc tempor. Nunc a est massa. Quisque sapien elit, imperdiet sit amet magna vel, sodales pretium nunc. Phasellus efficitur magna libero, sit amet euismod felis lacinia a. Nullam congue mi nisl, eget faucibus lacus tincidunt sit amet. Proin blandit quam non accumsan gravida. Nam mauris enim, tristique eu ante sed, tempus malesuada lorem. Pellentesque tincidunt posuere est ac iaculis.</p>', '', 1, '2017-09-24 07:56:06', NULL, 2, 'posts', 1),
(8, 'riazada', 'riazada', 'asasassa', 'upload/60f0ef63d22f1a69a1315d0cdb30a7f0.png', 1, '2017-09-24 08:20:20', NULL, 3, 'posts', 1),
(9, 'hello world', 'hello-world-1', 'hello world', 'upload/4effb7d24c2fb8f8261311bd348b769a.png', 1, '2017-09-24 15:15:13', NULL, 3, 'posts', 1),
(10, 'Home', 'home', 'Hello', '', 1, '2017-09-24 17:46:49', NULL, 3, 'pages', 1),
(11, 'home', 'home-1', '<p>asasasasasasassasasas</p>', '', 1, '2017-09-25 04:56:21', NULL, 2, 'pages', 1),
(12, 'About Us', 'about-us', '<p>HEllo</p>', 'upload/5d59783606a05c856aee105eee5859f1.jpg', 1, '2017-09-25 05:00:03', NULL, 1, 'pages', 1),
(13, 'about', 'about-1', '<p>HEllo</p>', 'upload/0783bc633f5ef151326ce27434064b64.jpg', 1, '2017-09-25 05:00:40', NULL, 1, 'pages', 1),
(31, 'about', 'about-2', '<p>Helloe</p>', '', 1, '2017-09-25 05:32:41', NULL, 1, 'pages', 1),
(32, 'Slider 1', 'Slider-1', '<p>asasasasas</p>', 'upload/3ff45e3d387c7dd9279838deb57a5cb4.jpg', 1, '2017-09-23 12:29:42', NULL, 3, 'slider', 1),
(33, 'Slider 2', 'Slider-2', '<p>asasasasas</p>', 'upload/3ce895abcd32febd43e5ddc9179022eb.jpg', 1, '2017-09-23 12:29:42', NULL, 1, 'slider', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Photo` text NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `Name`, `Photo`, `DateModified`, `PerformedBy`) VALUES
(1, 'Riazeda', '1.png', '2017-09-25 17:07:20', 1),
(4, 'Riazeda', '1.png', '2017-09-21 14:42:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbbldetail`
--

CREATE TABLE IF NOT EXISTS `tbbldetail` (
  `id` int(11) NOT NULL,
  `srNo` int(11) NOT NULL,
  `ContainerNo` varchar(255) NOT NULL,
  `Packages` int(11) NOT NULL,
  `SealNo` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbldetail`
--

INSERT INTO `tbbldetail` (`id`, `srNo`, `ContainerNo`, `Packages`, `SealNo`, `userId`, `dateAdded`, `dateUpdated`) VALUES
(2, 12087, 'TEMU9260133', 3158, '0000', 4, '2017-09-01 15:30:00', '0000-00-00 00:00:00'),
(3, 12992, 'WHSU5067533', 106, 'WHLB619623', 4, '2017-09-02 15:30:00', '0000-00-00 00:00:00'),
(4, 12993, 'WHSU5067533', 153, 'WHLB619623', 4, '2017-09-03 15:30:00', '0000-00-00 00:00:00'),
(5, 12994, 'WHSU5067533', 21, 'WHLB619623', 4, '2017-09-04 15:30:00', '0000-00-00 00:00:00'),
(6, 12995, 'WHLU5486431', 37, '00', 4, '2017-09-05 15:30:00', '0000-00-00 00:00:00'),
(7, 12995, 'WHLU7663462', 37, '00', 4, '2017-09-06 15:30:00', '0000-00-00 00:00:00'),
(8, 12995, 'TGHU6126950', 37, '00', 4, '2017-09-07 15:30:00', '0000-00-00 00:00:00'),
(9, 12995, 'TCNU4707764', 37, '00', 4, '2017-09-08 15:30:00', '0000-00-00 00:00:00'),
(10, 12995, 'FCIU8922939', 37, '00', 4, '2017-09-09 15:30:00', '0000-00-00 00:00:00'),
(11, 12995, 'WHLU5677063', 37, '00', 4, '2017-09-10 15:30:00', '0000-00-00 00:00:00'),
(12, 12995, 'WHLU5604257', 37, '00', 4, '2017-09-11 15:30:00', '0000-00-00 00:00:00'),
(13, 12995, 'TCNU5012463', 37, '00', 4, '2017-09-12 15:30:00', '0000-00-00 00:00:00'),
(14, 12995, 'WHLU5724587', 37, '00', 4, '2017-09-13 15:30:00', '0000-00-00 00:00:00'),
(15, 12995, 'WHLU5626723', 37, '00', 4, '2017-09-14 15:30:00', '0000-00-00 00:00:00'),
(16, 12996, 'WHLU0391879', 901, 'WHLB619644', 4, '2017-09-15 15:30:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblblmaster`
--

CREATE TABLE IF NOT EXISTS `tblblmaster` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `shipperName` varchar(255) NOT NULL,
  `shipperAddress1` text NOT NULL,
  `shipperAddress2` text NOT NULL,
  `shipperAddress3` text NOT NULL,
  `consigneeName` varchar(255) NOT NULL,
  `ConsigneeAddress1` text NOT NULL,
  `ConsigneeAddress2` text NOT NULL,
  `ConsigneeAddress3` text NOT NULL,
  `notifyName` varchar(255) NOT NULL,
  `NotifyAddress1` text NOT NULL,
  `NotifyAddress2` text NOT NULL,
  `NotifyAddress3` text NOT NULL,
  `VesselName` varchar(255) NOT NULL,
  `PortId` int(11) NOT NULL,
  `portOfDeleivery` varchar(255) NOT NULL,
  `portOfDischarge` varchar(255) NOT NULL,
  `Freight` varchar(255) NOT NULL,
  `NoOfOriginalBl` int(11) NOT NULL,
  `voyageNo` varchar(255) NOT NULL,
  `netWeight` float(11,2) NOT NULL,
  `grossWeight` float(11,2) NOT NULL,
  `measurement` float(11,2) NOT NULL,
  `shippingLine` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateNtime` datetime NOT NULL,
  `dataConvert` varchar(255) NOT NULL,
  `eFormNo` varchar(255) NOT NULL,
  `eFormDate` date NOT NULL,
  `Commodity` varchar(255) NOT NULL,
  `PackageTypeId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblBLmaster';

-- --------------------------------------------------------

--
-- Table structure for table `tblcountry`
--

CREATE TABLE IF NOT EXISTS `tblcountry` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcountry`
--

INSERT INTO `tblcountry` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People''s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'YU', 'Yugoslavia'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbldescription`
--

CREATE TABLE IF NOT EXISTS `tbldescription` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `Description` text NOT NULL,
  `userId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DatedUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblDescription';

-- --------------------------------------------------------

--
-- Table structure for table `tblemp`
--

CREATE TABLE IF NOT EXISTS `tblemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblEmp';

-- --------------------------------------------------------

--
-- Table structure for table `tblmarksnos`
--

CREATE TABLE IF NOT EXISTS `tblmarksnos` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `marksNo` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblMarksNos';

-- --------------------------------------------------------

--
-- Table structure for table `tblpackages`
--

CREATE TABLE IF NOT EXISTS `tblpackages` (
  `id` int(11) NOT NULL,
  `pkgeCode` varchar(255) NOT NULL,
  `packageType` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='tblPackages';

--
-- Dumping data for table `tblpackages`
--

INSERT INTO `tblpackages` (`id`, `pkgeCode`, `packageType`, `userId`, `dateAdded`, `dateUpdate`) VALUES
(3, 'asas', 'asasas', 4, '2017-09-22 10:08:29', '0000-00-00 00:00:00'),
(4, '231', '657', 1, '2017-09-23 23:05:09', '2017-09-23 23:05:35'),
(5, 'assa', 'ass', 1, '2017-09-24 01:25:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblportname`
--

CREATE TABLE IF NOT EXISTS `tblportname` (
  `id` int(11) NOT NULL,
  `portId` varchar(255) NOT NULL,
  `portName` varchar(255) NOT NULL,
  `countryId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datedUpdated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblportname`
--

INSERT INTO `tblportname` (`id`, `portId`, `portName`, `countryId`, `userId`, `dateAdded`, `datedUpdated`) VALUES
(2, 'asas', 'saas', 1, 1, '2017-09-23 23:12:55', '0000-00-00 00:00:00'),
(3, 'asas', 'asas', 16, 1, '2017-09-23 23:13:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Password`, `Role`, `Status`, `Photo`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(1, 'Demo', 'Demo', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, 1, '4.png', '2015-08-29 10:50:00', '2017-09-21 14:42:29', 4),
(6, 'asasas', 'asas', 'saasas@sss.com', '202cb962ac59075b964b07152d234b70', 1, 0, '', '2017-09-23 23:18:03', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `website_type`
--

CREATE TABLE IF NOT EXISTS `website_type` (
  `id` int(11) NOT NULL,
  `label` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `website_type`
--

INSERT INTO `website_type` (`id`, `label`, `name`, `image`, `status`) VALUES
(1, 'Riazeda', 'riazedaa', 'img/riazeda.png', 1),
(2, 'Transworld', 'transworld', 'img/transworld.png', 1),
(3, 'Wan Hai', 'wanhai', 'img/wanhai.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `defaultpage`
--
ALTER TABLE `defaultpage`
  ADD PRIMARY KEY (`default_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `postmeta`
--
ALTER TABLE `postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_type` (`website_type`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbbldetail`
--
ALTER TABLE `tbbldetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `tblblmaster`
--
ALTER TABLE `tblblmaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcountry`
--
ALTER TABLE `tblcountry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbldescription`
--
ALTER TABLE `tbldescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblemp`
--
ALTER TABLE `tblemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblmarksnos`
--
ALTER TABLE `tblmarksnos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpackages`
--
ALTER TABLE `tblpackages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblportname`
--
ALTER TABLE `tblportname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `website_type`
--
ALTER TABLE `website_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `defaultpage`
--
ALTER TABLE `defaultpage`
  MODIFY `default_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `postmeta`
--
ALTER TABLE `postmeta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbbldetail`
--
ALTER TABLE `tbbldetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tblblmaster`
--
ALTER TABLE `tblblmaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcountry`
--
ALTER TABLE `tblcountry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `tbldescription`
--
ALTER TABLE `tbldescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblemp`
--
ALTER TABLE `tblemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblmarksnos`
--
ALTER TABLE `tblmarksnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblpackages`
--
ALTER TABLE `tblpackages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblportname`
--
ALTER TABLE `tblportname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `website_type`
--
ALTER TABLE `website_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
