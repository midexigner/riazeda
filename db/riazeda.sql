-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2018 at 07:22 AM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `riazeda`
--

-- --------------------------------------------------------

--
-- Table structure for table `blform`
--

CREATE TABLE IF NOT EXISTS `blform` (
  `id` int(11) NOT NULL,
  `exportsid` int(11) NOT NULL,
  `shippername` varchar(255) NOT NULL,
  `shipperaddressone` text NOT NULL,
  `shipperaddresstwo` text NOT NULL,
  `shipperaddressthree` text NOT NULL,
  `consigneename` varchar(255) NOT NULL,
  `consigneeaddressone` text NOT NULL,
  `consigneeaddresstwo` text NOT NULL,
  `consigneeaddressthree` text NOT NULL,
  `notifyname` varchar(255) NOT NULL,
  `notifyaddressone` text NOT NULL,
  `notifyaddresstwo` text NOT NULL,
  `notifyaddressthree` text NOT NULL,
  `formeno` varchar(255) NOT NULL,
  `formedate` date NOT NULL,
  `vessel` varchar(255) NOT NULL,
  `voyageno` varchar(255) NOT NULL,
  `FreightType` varchar(255) NOT NULL,
  `other` varchar(255) NOT NULL,
  `portofloading` varchar(255) NOT NULL,
  `portofdischarge` varchar(255) NOT NULL,
  `placeofdelivery` varchar(255) NOT NULL,
  `blnumber` int(3) NOT NULL,
  `commodity` varchar(255) NOT NULL,
  `packagestype` varchar(255) NOT NULL,
  `containernumber` longtext NOT NULL,
  `sealnumber` longtext NOT NULL,
  `noofpackages` longtext NOT NULL,
  `marksno` longtext NOT NULL,
  `description` longtext NOT NULL,
  `netweight` varchar(255) NOT NULL,
  `grossweight` varchar(255) NOT NULL,
  `measurement` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `shippingline` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `blform`
--

INSERT INTO `blform` (`id`, `exportsid`, `shippername`, `shipperaddressone`, `shipperaddresstwo`, `shipperaddressthree`, `consigneename`, `consigneeaddressone`, `consigneeaddresstwo`, `consigneeaddressthree`, `notifyname`, `notifyaddressone`, `notifyaddresstwo`, `notifyaddressthree`, `formeno`, `formedate`, `vessel`, `voyageno`, `FreightType`, `other`, `portofloading`, `portofdischarge`, `placeofdelivery`, `blnumber`, `commodity`, `packagestype`, `containernumber`, `sealnumber`, `noofpackages`, `marksno`, `description`, `netweight`, `grossweight`, `measurement`, `client_id`, `shippingline`, `date_added`) VALUES
(1, 1, 'Rashad Wade', 'Sit omnis earum sint cupiditate deserunt voluptate voluptas aspernatur eveniet', '', '', 'Catherine Roberts', 'KARACHI, PAKISTAN', '', '', 'Brandon TRADERS', 'Sed alias ea quod amet duis asperiores id mollit libero nisi nulla temporibus corrupti Nam culpa eum eaque doloremque', 'Proident culpa ipsum est excepturi duis sunt omnis dolorum laborum Pariatur Architecto qui atque consectetur', 'Culpa et optio qui laborum laudantium consequatur Aperiam quia duis', 'Praesentium quia eos eaque vel ex dolore porro aliquip ad nihil do consequatur dolore ipsa veniam', '0000-00-00', 'Itaque molestiae beatae ullamco corporis', 'A cupidatat laboris enim quia odio quia quia aut cumque doloribus magni minima numquam', 'Collect At', 'Voluptate modi maiores et ut maxime voluptatem ratione dolor sint quis sunt et', 'AEJEA', 'CNLYG', 'VNSGN', 1, 'Soluta provident culpa in velit suscipit voluptas accusantium expedita neque nostrud exercitationem vel sequi sed ea rerum', 'CBC     ', '273', '190', 'Distinctio Quam et ex ipsam distinctio Aspernatur fugiat unde sed magna proident voluptas fugiat consectetur est', 'Excepturi dolore excepteur cumque animi aut voluptatibus aute sunt maxime officia ut maxime nihil architecto ex quo cupidatat', 'Dolore sint et non sit sit ea', 'Perferendis voluptatem sit magna deleniti quis quia veritatis dolores sed', 'Ratione rerum reiciendis laborum Dolorum cillum dolor facilis labore in consequuntur sapiente alias culpa non dolores est adipisci error', 'Ipsam et aut qui temporibus in rerum sapiente quia asperiores temporibus excepturi mollitia ipsam officia', 14, 2, '0000-00-00 00:00:00'),
(2, 2, 'Recron (MALAYSIA) SDH.BHD.', 'LEVEL 9, WISMA GOLDHILL 67, JALAN RAJA CHULAN 50200, KUALA LUMPUR, MALAYSIA', '', '', 'TO THE ORDER OF HABIB MEROPOLITAN BANK LTD', 'KARACHI, PAKISTAN', '', '', 'J.K TRADERS', '261/B 1ST FLOOR, LATIF CLOTH MARKET, M.A. JINNAH ROAD, KARACHI, PAKISTAN', '', '', '0457A35691', '0000-00-00', 'LOS ANGELES TRADER', '001W', 'Prepaid At', '', 'PORT KLANG, MALAYSIA', 'KARACHI PORT PAKISTAN', 'KARACHI PORT PAKISTAN', 3, 'Qui quia voluptatem in et voluptate porro quis neque voluptas magna sapiente sapiente eveniet ex amet amet iure laboris voluptatem', 'PKT', 'FCL / FCL', 'WHLU5716262 40SD96 WHLA213806', '1 CTR ( 736 BOXS)', 'NIL', 'SHIPPER''S PACK LOAD COUNT & SEAL"\n"SAID TO CONTAIN"\n"FREIGHT PR:PAID"\nSAY: ONE CONTAINER ONLY', 'Quia quam sed debitis proident porro quia maxime officiis aliquip do quo exercitationem nulla voluptatibus non', '28,856.000', '50.0000', 14, 0, '0000-00-00 00:00:00'),
(3, 3, 'Barclay Williams', 'Veritatis laboriosam dolor cupiditate culpa', 'Aut nulla ex officiis maxime Nam qui voluptas', 'Aliqua Nisi minima dolorem ut aut unde commodo odit illo eiusmod vitae accusantium est non itaque et mollitia et', 'Lyle Cherry', 'Error eius sequi fuga Est culpa eu', 'Assumenda elit ut incidunt quo velit accusantium velit eos vitae vitae', 'Illum qui qui voluptate ab', 'Cathleen Moran', 'Iure et laboriosam ea sed temporibus dolor duis cum obcaecati odio quasi repellendus Mollit voluptas excepteur in id', 'Deleniti quisquam expedita amet ut sunt delectus temporibus quia est aut harum aliquid in', 'Ea qui reiciendis sint irure eius labore aut voluptas qui praesentium facere iure quisquam error quibusdam corrupti sit reprehenderit voluptas', 'Sed ex perspiciatis magni occaecat inventore praesentium', '0000-00-00', 'Dicta quas magni veniam qui nostrud et quidem illo', 'Et sint facilis illum impedit quod incidunt itaque nemo nostrud consequuntur consequatur Sed veritatis est amet incidunt', 'Collect At', 'Ea quibusdam do dolorum quidem eaque ipsum do mollitia exercitation do Nam dolorum pariatur', 'MYBKI', 'IQUQR', 'CNFGC', 3, 'Sunt ut ea mollit dolore provident omnis laudantium sit sit vero qui nisi tenetur modi fugit natus in', 'ROL     ', '243", "447", "972", "892", "238', '5", "890", "654", "600", "413', 'Reprehenderit quas quas laudantium aut cupidatat neque nesciunt impedit quis impedit nemo", "Consequatur magna quia veritatis reprehenderit architecto aut id est obcaecati nihil culpa culpa necessitatibus mollitia consequatur Odio id enim", "Eius iste ex optio quis fugit corporis quos nihil pariatur Ut laborum", "Id ea ipsa in sint labore in maiores ea voluptatem illo", "Reiciendis fugiat enim quis ducimus aut consectetur', 'Praesentium dolorum earum quia alias mollit ut esse minima similique nobis adipisci ipsum in recusandae Dolor sed debitis", "Est enim veritatis cillum excepturi et in doloremque veritatis expedita aspernatur aliquam aut quia neque", "Qui tempore ex labore omnis omnis sit ea iste iste aut iusto", "Laborum Deserunt voluptatem soluta nesciunt corporis dolor", "Fugiat minima iste cumque sit omnis sit do', 'Nulla earum laborum Ut maxime nostrud magnam exercitationem ex vitae ipsum consequatur Quasi facere magni natus duis culpa cupiditate vel", "Rem ab in aut cupiditate ex aliquip occaecat", "Sed aliquip ut quia quaerat", "Sed itaque reprehenderit possimus aliqua Iusto sit eius id", "Et quam possimus voluptates sit quam natus commodi assumenda laborum proident reiciendis maxime fugit aspernatur quasi dicta aut esse', 'Eiusmod a et ut maiores tempora dolores ut qui', 'Et autem sit ex duis cillum consequatur', 'Dolorum consequatur nobis quia suscipit non quia dolore eum velit', 13, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `defaultpage`
--

CREATE TABLE IF NOT EXISTS `defaultpage` (
  `default_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `default_value` varchar(30) NOT NULL,
  `website_type` int(11) NOT NULL,
  `value` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `defaultpage`
--

INSERT INTO `defaultpage` (`default_id`, `post_id`, `default_value`, `website_type`, `value`) VALUES
(1, 11, 'Home', 1, 1),
(2, 38, 'Blog', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `exportsearchengine`
--

CREATE TABLE IF NOT EXISTS `exportsearchengine` (
  `id` int(11) NOT NULL,
  `cargotype` varchar(255) NOT NULL,
  `bookingstatus` varchar(255) NOT NULL,
  `cargostatus` varchar(255) NOT NULL,
  `commodity` varchar(255) NOT NULL,
  `pol` varchar(255) NOT NULL,
  `pod` varchar(255) NOT NULL,
  `requestform` varchar(255) NOT NULL,
  `shipper` varchar(255) NOT NULL,
  `forwarder` varchar(255) NOT NULL,
  `clearingagent` varchar(255) NOT NULL,
  `emptyfrom` varchar(255) NOT NULL,
  `total20` int(11) NOT NULL,
  `total40` int(11) NOT NULL,
  `totalrf` int(11) NOT NULL,
  `referremarks` text NOT NULL,
  `shippingline` varchar(255) NOT NULL,
  `status` enum('incomplete','complete') NOT NULL DEFAULT 'incomplete',
  `clientsid` int(11) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateupdate` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `exportsearchengine`
--

INSERT INTO `exportsearchengine` (`id`, `cargotype`, `bookingstatus`, `cargostatus`, `commodity`, `pol`, `pod`, `requestform`, `shipper`, `forwarder`, `clearingagent`, `emptyfrom`, `total20`, `total40`, `totalrf`, `referremarks`, `shippingline`, `status`, `clientsid`, `dateadded`, `dateupdate`) VALUES
(1, 'Refer', 'Collect', 'Proident ut in maiores minima voluptas eum vero ad', 'Qui necessitatibus lorem libero adipisci aut magnam rerum dignissimos ratione omnis voluptatem beatae magnam sint esse', 'Libero nisi ullamco saepe et nostrum aut laudantium', 'Voluptatum dignissimos sint quia excepteur distinctio Beatae dolores enim ullam laborum amet anim alias consequatur laborum Duis', 'Self', 'Accusamus odit ipsum libero nihil', 'Dolorem est optio quis inventore deserunt veritatis elit mollitia est pariatur Veniam ullamco eos ipsum optio labore illo eius aliquam', '', 'Laboris autem magni beatae et quia pariatur Sit ea commodo alias quam non sapiente esse nemo qui', 0, 0, 0, 'Enim suscipit aut accusamus eveniet sequi id aute dolorem dicta sunt nostrud omnis facere corrupti', '2', 'complete', 14, '2018-01-05 11:27:16', NULL),
(2, 'Dry', 'Collect', 'Deserunt laudantium sint aut officia dolor aut alias molestiae', 'Qui soluta et tempore tempore sed veritatis nihil tempor occaecat ut', 'Sit commodi et vel dolorem ipsam est voluptate iure qui nemo in fugit sunt porro quia', 'Quis molestias perferendis repudiandae ad voluptatibus quae totam cupiditate incidunt consequatur Nulla est sint similique adipisci aperiam et necessitatibus', 'Shiper', 'Anim quo omnis impedit in aute consequatur est nisi iste error pariatur Veritatis sunt', 'Similique minim ut vel sint commodi similique autem', '', 'Sit veniam consequuntur facere ut sapiente commodi et voluptates deleniti optio', 0, 0, 0, 'Dolore explicabo Ut itaque aut laborum eos praesentium iste ut', '1', 'complete', 14, '2018-01-05 11:30:03', NULL),
(3, 'Refer', 'Prepaid', 'Mollitia aut sit modi odit', 'Ex vel aperiam voluptatem ex', 'Repellendus Est minim optio tempora', 'Et quia recusandae Ad et sit nostrud fugit aut et voluptas mollit debitis vel sit impedit', 'Self', 'Maxime voluptatem ea dolor minus fugit nobis aliquid ducimus do doloremque nihil ea aliquip et labore rerum ipsa in', 'Esse similique consequatur quas in culpa anim elit et et non atque earum facere labore minim molestias in laboriosam sed', '', 'Est dolorem amet deserunt cumque earum accusantium ut in enim ea velit nobis totam excepteur non ullam tempora provident odio', 0, 0, 0, 'Aut velit quo voluptas est illum rerum architecto animi expedita culpa et sunt consequuntur quod suscipit alias non', '1', 'complete', 13, '2018-01-19 04:38:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heading`
--

CREATE TABLE IF NOT EXISTS `heading` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `col1` varchar(255) NOT NULL,
  `col2` varchar(255) NOT NULL,
  `head_id` int(11) NOT NULL,
  `sort` tinyint(1) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime DEFAULT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heading`
--

INSERT INTO `heading` (`id`, `name`, `status`, `col1`, `col2`, `head_id`, `sort`, `dateAdded`, `dateUpdate`, `user`) VALUES
(1, 'CY DELEIVERY T H C CHARGES (GENERAL)', 1, '20"', '40"', 1, 0, '2017-12-27 15:24:38', '2017-12-27 16:00:23', 1),
(2, 'CY DELEIVERY T H C CHARGES (HAZARDOUS)', 1, '20"', '40"', 2, 0, '2017-12-27 16:09:42', '2017-12-27 16:10:28', 1),
(3, 'CY REFFER T H C CHARGES', 1, '20"', '40"', 3, 0, '2017-12-27 16:11:51', NULL, 1),
(4, 'test', 1, '1212', 'asasas', 1, 0, '2018-01-11 11:55:03', '2018-01-11 12:24:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `heads`
--

CREATE TABLE IF NOT EXISTS `heads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `col1` varchar(255) NOT NULL,
  `col2` varchar(255) NOT NULL,
  `sort` tinyint(1) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime DEFAULT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `heads`
--

INSERT INTO `heads` (`id`, `name`, `status`, `col1`, `col2`, `sort`, `dateAdded`, `dateUpdate`, `user`) VALUES
(1, 'CY / CY Charges', 1, '20"', '40"', 0, '2017-12-27 15:24:38', '2017-12-27 16:00:23', 1),
(2, 'DESTUFFING CHARGES', 1, '20"', '40"', 0, '2017-12-27 16:09:42', '2017-12-27 16:10:28', 1),
(3, 'DETENTION CHARGES WAN HAI (DRY)', 1, '20"', '40"', 0, '2017-12-27 16:11:51', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `loginattempts`
--

CREATE TABLE IF NOT EXISTS `loginattempts` (
  `id` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `attempts` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `loginattempts`
--

INSERT INTO `loginattempts` (`id`, `ip`, `attempts`, `username`, `last_login`) VALUES
(1, '::1', 1, 'idrees.cis', '2018-01-05 12:56:08');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `menu_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `menu_label` varchar(255) NOT NULL,
  `menu_parent` int(11) NOT NULL DEFAULT '0',
  `menu_status` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `menu_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `menu_updated` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `post_id`, `menu_label`, `menu_parent`, `menu_status`, `userId`, `menu_created`, `menu_updated`) VALUES
(1, 11, 'Home', 0, 1, 1, '2018-01-12 11:21:02', NULL),
(2, 12, 'About Us', 0, 1, 1, '2018-01-12 14:18:13', NULL),
(3, 40, 'Services', 0, 1, 1, '2018-01-12 14:19:00', NULL),
(4, 41, 'Vessel Scheduler', 0, 1, 1, '2018-01-12 14:19:22', NULL),
(5, 39, 'Contact Us', 0, 1, 1, '2018-01-12 14:20:20', NULL),
(6, 41, 'Import', 4, 1, 1, '2018-01-12 14:23:22', NULL),
(7, 41, 'Export', 6, 1, 1, '2018-01-12 14:41:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `option_id` int(11) NOT NULL,
  `option_name` varchar(191) NOT NULL,
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'title', 'Orem', 'yes'),
(2, 'logo', 'CIShop-logo.png', 'yes'),
(3, 'favicon', 'Artboard_1.png', 'yes'),
(4, 'loginBackground', 'banner-12.jpg', 'yes'),
(5, 'address', '[{"address":"124\\/6 Street, Country","phone":"00 000 000 000","fax":"00 000 000 000","email":"admin@admin.com"}]', 'yes'),
(6, 'googleAnalytics', '<script>\r\n          //Google Analytics Scriptfffffffffffffffffffffffssssfffffs\r\n       </script>', 'yes'),
(7, 'metaKeywords', 'Genius,Ocean,Sea,Etc,Genius,Ocean,SeaGenius,Ocean,Sea,Etc,Genius,Ocean,SeaGenius,Ocean,Sea,Etc,Genius,Ocean,Sea', 'yes'),
(8, 'metaDescription', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `postmeta`
--

CREATE TABLE IF NOT EXISTS `postmeta` (
  `meta_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_value` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postmeta`
--

INSERT INTO `postmeta` (`meta_id`, `post_id`, `meta_value`) VALUES
(1, 30, 'page-home.php'),
(2, 31, 'page-about.php'),
(8, 2, ''),
(9, 34, 'page-contact.php'),
(10, 12, 'default'),
(11, 38, 'default'),
(12, 39, 'page-contact.php'),
(13, 40, 'page-service.php'),
(14, 41, 'default'),
(15, 42, 'default'),
(16, 42, 'default'),
(17, 42, 'default'),
(18, 43, 'default');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `images` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_updated` datetime DEFAULT NULL,
  `website_type` int(11) NOT NULL,
  `post_type` varchar(50) NOT NULL,
  `menu_orders` tinyint(10) NOT NULL DEFAULT '0',
  `status` varchar(20) NOT NULL DEFAULT 'publish'
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='posts (type mention in posts or page website) or website_type: (mention in 3 difference  website )';

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `slug`, `body`, `images`, `userId`, `created_at`, `created_updated`, `website_type`, `post_type`, `menu_orders`, `status`) VALUES
(1, 'Hello Worlds', 'hello-worlds', '<p>The quick brown fox jumped over the lazy dog.</p>', 'upload/0b17ff3c7ecb07ac8388e3027c487500.jpg', 1, '2017-09-23 12:20:39', '2017-09-30 02:42:06', 1, 'posts', 0, '1'),
(11, 'Home', 'home', '<p>asasasasasasassasasas</p>', '', 1, '2017-09-25 04:56:21', NULL, 1, 'pages', 0, '1'),
(12, 'About Us', 'about-us', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 'upload/5d59783606a05c856aee105eee5859f1.jpg', 1, '2017-09-25 05:00:03', '2017-12-11 15:12:46', 1, 'pages', 0, '1'),
(33, 'PROVIDING FIRST CLASS <br> LOGISTIC SERVICES', 'providing-first-class-br-logistic-services', '<div class="container">\r\n<div class="row">\r\n<div class="banner-text">\r\n<div class="animated fadeInRight">\r\n<h1>PROVIDING FIRST CLASS <br /> LOGISTIC SERVICES</h1>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>\r\n<div class="btn-banner"><a class="btn solid-btn" href="service.html">our services</a> <a class="btn solid-btn">get a quote</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'upload/61cea14bf9cba1bc9512b01a2c5cb1cc.jpg', 1, '2017-09-23 12:29:42', '2018-01-11 17:06:02', 1, 'slider', 0, '1'),
(37, 'WORLD CLASS <br> SHIPPING SERVICES', 'world-class-br-shipping-services', '<div class="container">\r\n<div class="row">\r\n<div class="banner-text f-w text-center w-color">\r\n<div class="animated fadeInUp">\r\n<h1 class="w-heading">WORLD CLASS <br /> SHIPPING SERVICES</h1>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>\r\n<div class="btn-banner"><a class="btn solid-btn" href="service.html">our services</a> <a class="btn trns-btn-w">get a quote</a></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'upload/bbe6ced13b569a79c79b2467dfd2f935.jpg', 1, '2017-10-09 09:00:10', '2018-01-11 17:07:48', 1, 'slider', 0, '1'),
(39, 'Contact Us', 'contact-us', '<p>[heading] Hello World[/heading]</p>', '', 1, '2017-12-11 11:13:20', '2018-01-15 15:30:40', 1, 'pages', 0, '1'),
(40, 'Services', 'services', '', '', 1, '2017-12-11 12:17:19', NULL, 1, 'pages', 0, '1'),
(41, 'Vessel Scheduler', 'vessel-scheduler', '', '', 1, '2017-12-18 04:37:43', NULL, 1, 'pages', 0, '1'),
(42, '	Services', 'services-1', '', '', 1, '2018-01-23 10:05:13', NULL, 1, 'pages', 0, '1'),
(43, '	Services', 'services-2', '', '', 1, '2018-01-23 10:05:31', NULL, 1, 'pages', 0, '1');

-- --------------------------------------------------------

--
-- Table structure for table `registered`
--

CREATE TABLE IF NOT EXISTS `registered` (
  `id` int(11) NOT NULL,
  `name` varchar(190) NOT NULL,
  `email` varchar(190) NOT NULL,
  `password` varchar(32) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `status` enum('inactive','active') NOT NULL DEFAULT 'inactive',
  `hostname` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='user can register with verification by link';

--
-- Dumping data for table `registered`
--

INSERT INTO `registered` (`id`, `name`, `email`, `password`, `contact`, `username`, `hash`, `status`, `hostname`, `created_at`, `updated_at`) VALUES
(10, 'Mubashir', 'mubashir.cis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '3322398589', 'mubashir.cis', 'f64eac11f2cd8f0efa196f8ad173178e', 'inactive', '', '2017-12-29 04:17:46', NULL),
(11, 'Haris', 'cis.harisali@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '123456789', 'haris.cis', '93db85ed909c13838ff95ccfa94cebd9', 'active', '', '2017-12-29 04:31:42', NULL),
(13, 'Muhammad Idrees', 'midrees.cis@gmail.com', '25f9e794323b453885f5181f1b624d0b', '03242340583', 'midrees.cis', '8b16ebc056e613024c057be590b542eb', 'active', 'CIS-PC', '2017-12-29 07:13:48', NULL),
(14, 'Muhammad Idrees', 'idrees@cloud-innovator.com', '25f9e794323b453885f5181f1b624d0b', '03242340583', 'idrees.cis', '0efe32849d230d7f53049ddc4a4b0c60', 'active', 'CIS-PC', '2017-12-29 07:15:24', NULL),
(15, 'Muhammad Salman', 'salmanm.cis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1234567890', 'salman.cis', '821fa74b50ba3f7cba1e6c53e8fa6845', 'inactive', 'CIS-PC', '2017-12-29 07:43:47', NULL),
(16, 'test ', 'saqibshaikh.cis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1244123123123123', 'saqibshaikh', 'd1c38a09acc34845c6be3a127a5aacaf', 'active', 'CIS-PC', '2017-12-29 07:48:44', NULL),
(17, 'waqas', 'waqasahmed.cis@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '34567890', 'waqas', 'e6cb2a3c14431b55aa50c06529eaa21b', 'active', 'CIS-PC', '2017-12-29 07:53:08', NULL),
(18, 'zxzxz', 'zxzxzx@asa.com', 'ddcf4466a7ee29215b8595e30b8bfbe4', 'zxzx', 'zxzx', 'e555ebe0ce426f7f9b2bef0706315e0c', 'inactive', 'CIS-PC', '2018-01-05 06:12:04', NULL),
(19, 'Dane Copeland', 'dygizuvuko@hotmail.com', 'f3ed11bbdb94fd9ebdefbaf646ab94d3', '+834-62-6778103', 'farijyqyv', '5807a685d1a9ab3b599035bc566ce2b9', 'inactive', 'CIS-PC', '2018-01-19 11:31:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rows`
--

CREATE TABLE IF NOT EXISTS `rows` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `col1` varchar(255) NOT NULL,
  `col2` varchar(255) NOT NULL,
  `heading_id` int(11) NOT NULL,
  `sort` tinyint(1) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime DEFAULT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rows`
--

INSERT INTO `rows` (`id`, `name`, `status`, `col1`, `col2`, `heading_id`, `sort`, `dateAdded`, `dateUpdate`, `user`) VALUES
(1, 'CY DELEIVERY T H ', 1, '20"', '40"', 1, 0, '2017-12-27 15:24:38', NULL, 1),
(2, 'CY DELEIVERY T H C ', 1, '20"', '40"', 2, 0, '2017-12-27 16:09:42', NULL, 1),
(3, 'CY REFFER T H ', 1, '20"', '40"', 3, 0, '2017-12-27 16:11:51', NULL, 1),
(4, 'assasas', 1, '1212', 'asasas', 1, 0, '2018-01-11 11:55:03', '2018-01-11 15:44:49', 1),
(5, 'asasas', 1, 'asa', 'asass', 4, 0, '2018-01-11 14:35:30', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `ID` int(11) NOT NULL,
  `Name` text NOT NULL,
  `Photo` text NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`ID`, `Name`, `Photo`, `DateModified`, `PerformedBy`) VALUES
(1, 'Riazeda', '1.png', '2017-12-18 09:30:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbbldetail`
--

CREATE TABLE IF NOT EXISTS `tbbldetail` (
  `id` int(11) NOT NULL,
  `srNo` int(11) NOT NULL,
  `ContainerNo` varchar(255) NOT NULL,
  `Packages` int(11) NOT NULL,
  `SealNo` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbbldetail`
--

INSERT INTO `tbbldetail` (`id`, `srNo`, `ContainerNo`, `Packages`, `SealNo`, `userId`, `dateAdded`, `dateUpdated`) VALUES
(2, 12087, 'TEMU9260133', 3158, '0000', 4, '2017-09-01 15:30:00', '0000-00-00 00:00:00'),
(3, 12992, 'WHSU5067533', 106, 'WHLB619623', 4, '2017-09-02 15:30:00', '0000-00-00 00:00:00'),
(4, 12993, 'WHSU5067533', 153, 'WHLB619623', 4, '2017-09-03 15:30:00', '0000-00-00 00:00:00'),
(5, 12994, 'WHSU5067533', 21, 'WHLB619623', 4, '2017-09-04 15:30:00', '0000-00-00 00:00:00'),
(6, 12995, 'WHLU5486431', 37, '00', 4, '2017-09-05 15:30:00', '0000-00-00 00:00:00'),
(7, 12995, 'WHLU7663462', 37, '00', 4, '2017-09-06 15:30:00', '0000-00-00 00:00:00'),
(8, 12995, 'TGHU6126950', 37, '00', 4, '2017-09-07 15:30:00', '0000-00-00 00:00:00'),
(9, 12995, 'TCNU4707764', 37, '00', 4, '2017-09-08 15:30:00', '0000-00-00 00:00:00'),
(10, 12995, 'FCIU8922939', 37, '00', 4, '2017-09-09 15:30:00', '0000-00-00 00:00:00'),
(11, 12995, 'WHLU5677063', 37, '00', 4, '2017-09-10 15:30:00', '0000-00-00 00:00:00'),
(12, 12995, 'WHLU5604257', 37, '00', 4, '2017-09-11 15:30:00', '0000-00-00 00:00:00'),
(13, 12995, 'TCNU5012463', 37, '00', 4, '2017-09-12 15:30:00', '0000-00-00 00:00:00'),
(14, 12995, 'WHLU5724587', 37, '00', 4, '2017-09-13 15:30:00', '0000-00-00 00:00:00'),
(15, 12995, 'WHLU5626723', 37, '00', 4, '2017-09-14 15:30:00', '0000-00-00 00:00:00'),
(16, 12996, 'WHLU0391879', 901, 'WHLB619644', 4, '2017-09-15 15:30:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblblmaster`
--

CREATE TABLE IF NOT EXISTS `tblblmaster` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `shipperName` varchar(255) NOT NULL,
  `shipperAddress1` text NOT NULL,
  `shipperAddress2` text NOT NULL,
  `shipperAddress3` text NOT NULL,
  `consigneeName` varchar(255) NOT NULL,
  `ConsigneeAddress1` text NOT NULL,
  `ConsigneeAddress2` text NOT NULL,
  `ConsigneeAddress3` text NOT NULL,
  `notifyName` varchar(255) NOT NULL,
  `NotifyAddress1` text NOT NULL,
  `NotifyAddress2` text NOT NULL,
  `NotifyAddress3` text NOT NULL,
  `VesselName` varchar(255) NOT NULL,
  `PortId` int(11) NOT NULL,
  `portOfDeleivery` varchar(255) NOT NULL,
  `portOfDischarge` varchar(255) NOT NULL,
  `Freight` varchar(255) NOT NULL,
  `NoOfOriginalBl` int(11) NOT NULL,
  `voyageNo` varchar(255) NOT NULL,
  `netWeight` float(11,2) NOT NULL,
  `grossWeight` float(11,2) NOT NULL,
  `measurement` float(11,2) NOT NULL,
  `shippingLine` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateNtime` datetime NOT NULL,
  `dataConvert` varchar(255) NOT NULL,
  `eFormNo` varchar(255) NOT NULL,
  `eFormDate` date NOT NULL,
  `Commodity` varchar(255) NOT NULL,
  `PackageTypeId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblBLmaster';

-- --------------------------------------------------------

--
-- Table structure for table `tblcountry`
--

CREATE TABLE IF NOT EXISTS `tblcountry` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblcountry`
--

INSERT INTO `tblcountry` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People''s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'YU', 'Yugoslavia'),
(244, 'ZR', 'Zaire'),
(245, 'ZM', 'Zambia'),
(246, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `tbldescription`
--

CREATE TABLE IF NOT EXISTS `tbldescription` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `Description` text NOT NULL,
  `userId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DatedUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblDescription';

-- --------------------------------------------------------

--
-- Table structure for table `tblemp`
--

CREATE TABLE IF NOT EXISTS `tblemp` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblEmp';

-- --------------------------------------------------------

--
-- Table structure for table `tblmarksnos`
--

CREATE TABLE IF NOT EXISTS `tblmarksnos` (
  `id` int(11) NOT NULL,
  `SrNo` int(11) NOT NULL,
  `marksNo` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `DateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `DateUpdated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='tblMarksNos';

-- --------------------------------------------------------

--
-- Table structure for table `tblpackages`
--

CREATE TABLE IF NOT EXISTS `tblpackages` (
  `id` int(11) NOT NULL,
  `pkgeCode` varchar(255) NOT NULL,
  `packageType` varchar(255) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dateUpdate` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='tblPackages';

--
-- Dumping data for table `tblpackages`
--

INSERT INTO `tblpackages` (`id`, `pkgeCode`, `packageType`, `userId`, `dateAdded`, `dateUpdate`) VALUES
(3, 'asas', 'asasas', 4, '2017-09-22 10:08:29', '0000-00-00 00:00:00'),
(4, '231', '657', 1, '2017-09-23 23:05:09', '2017-09-23 23:05:35'),
(5, 'assa', 'ass', 1, '2017-09-24 01:25:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tblportname`
--

CREATE TABLE IF NOT EXISTS `tblportname` (
  `id` int(11) NOT NULL,
  `portId` varchar(255) NOT NULL,
  `portName` varchar(255) NOT NULL,
  `countryId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `dateAdded` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `datedUpdated` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tblportname`
--

INSERT INTO `tblportname` (`id`, `portId`, `portName`, `countryId`, `userId`, `dateAdded`, `datedUpdated`) VALUES
(2, 'asas', 'saas', 1, 1, '2017-09-23 23:12:55', '0000-00-00 00:00:00'),
(3, 'asas', 'asas', 16, 1, '2017-09-23 23:13:05', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(11) NOT NULL,
  `FirstName` text NOT NULL,
  `LastName` text NOT NULL,
  `EmailAddress` text NOT NULL,
  `Password` char(32) NOT NULL,
  `Role` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `Photo` text NOT NULL,
  `DateAdded` datetime NOT NULL,
  `DateModified` datetime NOT NULL,
  `PerformedBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `FirstName`, `LastName`, `EmailAddress`, `Password`, `Role`, `Status`, `Photo`, `DateAdded`, `DateModified`, `PerformedBy`) VALUES
(1, 'Demo', 'Demo', 'admin@admin.com', '202cb962ac59075b964b07152d234b70', 1, 1, '1.png', '2015-08-29 10:50:00', '2017-12-18 09:31:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `website_type`
--

CREATE TABLE IF NOT EXISTS `website_type` (
  `id` int(11) NOT NULL,
  `label` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `image` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `website_type`
--

INSERT INTO `website_type` (`id`, `label`, `name`, `image`, `status`) VALUES
(1, 'Riazeda', 'riazedaa', 'img/riazeda.png', 1),
(2, 'Transworld', 'transworld', 'img/transworld.png', 1),
(3, 'Wan Hai', 'wanhai', 'img/wanhai.png', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blform`
--
ALTER TABLE `blform`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `defaultpage`
--
ALTER TABLE `defaultpage`
  ADD PRIMARY KEY (`default_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `exportsearchengine`
--
ALTER TABLE `exportsearchengine`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heading`
--
ALTER TABLE `heading`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heads`
--
ALTER TABLE `heads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loginattempts`
--
ALTER TABLE `loginattempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `postmeta`
--
ALTER TABLE `postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_type` (`website_type`);

--
-- Indexes for table `registered`
--
ALTER TABLE `registered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rows`
--
ALTER TABLE `rows`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbbldetail`
--
ALTER TABLE `tbbldetail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `tblblmaster`
--
ALTER TABLE `tblblmaster`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcountry`
--
ALTER TABLE `tblcountry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbldescription`
--
ALTER TABLE `tbldescription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblemp`
--
ALTER TABLE `tblemp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblmarksnos`
--
ALTER TABLE `tblmarksnos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpackages`
--
ALTER TABLE `tblpackages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblportname`
--
ALTER TABLE `tblportname`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `website_type`
--
ALTER TABLE `website_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blform`
--
ALTER TABLE `blform`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `defaultpage`
--
ALTER TABLE `defaultpage`
  MODIFY `default_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `exportsearchengine`
--
ALTER TABLE `exportsearchengine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `heading`
--
ALTER TABLE `heading`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `heads`
--
ALTER TABLE `heads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `loginattempts`
--
ALTER TABLE `loginattempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `postmeta`
--
ALTER TABLE `postmeta`
  MODIFY `meta_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `registered`
--
ALTER TABLE `registered`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `rows`
--
ALTER TABLE `rows`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbbldetail`
--
ALTER TABLE `tbbldetail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tblblmaster`
--
ALTER TABLE `tblblmaster`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblcountry`
--
ALTER TABLE `tblcountry`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `tbldescription`
--
ALTER TABLE `tbldescription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblemp`
--
ALTER TABLE `tblemp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblmarksnos`
--
ALTER TABLE `tblmarksnos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tblpackages`
--
ALTER TABLE `tblpackages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tblportname`
--
ALTER TABLE `tblportname`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `website_type`
--
ALTER TABLE `website_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
